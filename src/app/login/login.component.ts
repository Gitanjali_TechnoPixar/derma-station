import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from './login.service';
import { ConstantValues } from '../shared/enums/constant-values.enum';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  submitted:boolean=false;
  response:any;

  constructor( private fb:FormBuilder,
    private spinner: NgxSpinnerService,
    private router: Router,
    private toastr: ToastrService,
    private loginService:LoginService
    ) {}

  ngOnInit(): void {
    this.login();
  }
  login(){
    this.loginForm = this.fb.group({
      inputValue: new FormControl('',[Validators.required, Validators.email]),
      password: new FormControl('',[Validators.required]),
      deviceType: [ConstantValues.consDeviceType],
      deviceToken: [ConstantValues.consDeviceToken],
    })
  }

  get getFormControl() {
    return this.loginForm['controls'];
  }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.spinner.hide();
      // this.toastr.error('Invalid Form, Kindaly Fill The Form');
      return;
    }
    else{
      this.loginService.postLoginData(this.loginForm.value).subscribe(
        res=>{
          this.response=res;
          if (this.response['status'] == true) {
            this.spinner.hide();
            this.toastr.success(this.response['message']);
            this.router.navigateByUrl('/home');
          } 
          else if(this.response['status'] == false){
            this.spinner.hide();
            this.toastr.error(this.response['message']);
          }
          else{
            this.spinner.hide();
            this.toastr.error(this.response['message']);
          }
        }
      )
    }
  }
}
