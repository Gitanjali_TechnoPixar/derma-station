import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public baseUrl = environment.baseUrl

  constructor(private httpClient:HttpClient) { }

  postLoginData(data){
    return this.httpClient.post(`${this.baseUrl}Auth/Login`, data)
  }
}
