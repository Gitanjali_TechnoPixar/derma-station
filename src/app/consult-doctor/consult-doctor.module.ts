import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultDoctorRoutingModule } from './consult-doctor-routing.module';
import { ConsultDoctorComponent } from './consult-doctor.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ConsultDoctorComponent],
  imports: [CommonModule, ConsultDoctorRoutingModule, SharedModule],
})
export class ConsultDoctorModule {}
