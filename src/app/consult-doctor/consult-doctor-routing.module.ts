import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultDoctorComponent } from './consult-doctor.component';

const routes: Routes = [
  {
    path: '',
    component: ConsultDoctorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultDoctorRoutingModule {}
