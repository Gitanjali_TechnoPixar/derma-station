import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { NgxSpinnerService } from 'ngx-spinner';
import { HomeService } from './home.service';
import { environment } from 'src/environments/environment.prod';
import { AddToBagService, LanguageService } from '../shared/services';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddToBagPopupComponent } from '../shared/components/add-to-bag-popup/add-to-bag-popup.component';
import { CartService } from '../cart/cart.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  baseImageUrl = environment.baseImageUrl;

  currentLang: any;
  cartId: any;
  data = {};
  totalCartItems: any;
  cartItems:any;
  cart:any;
  quantityValueForcart: number = 1;
  getResponse:any;
  recommendedProductListResponse: any;
  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 2,
      },
      740: {
        items: 3,
      },
      940: {
        items: 3,
      },
    },
    nav: false,
  };
  constructor(private homeService: HomeService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    public translate: TranslateService,
    private router: Router,
    private addToBagService: AddToBagService,
    private toastr: ToastrService,
    public dialog: MatDialog,
    private cartService: CartService,

  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    this.spinner.show();
  }

  ngOnInit(): void {
    this.recommendedProductList();
  }

  switchLanguage(language: string) {
    this.translate.use(language);
  }
  recommendedProductList() {
    this.spinner.show();
    this.homeService.getRecommendedProductList().subscribe(
      res => {
        this.recommendedProductListResponse = res['data'];
        this.spinner.hide();
      }
    )
  }

  // Add to Cart
  // openAddToCart(template: TemplateRef<any>, productData: any) {

  openAddToCart(productData: any) {
    // this.modalRef = this.modalService.show(template);
    // console.log(productData);
    this.cartId = localStorage.getItem('cartId');
    if (this.cartId == null) {
      this.data = {
        cartId: 0,
        quantity: this.quantityValueForcart,
        price: productData.price,
        productId: productData.productId,
      };
    }
    else {
      this.data = {
        // cartId: 0,
        cartId: localStorage.getItem('cartId'),
        quantity: this.quantityValueForcart,
        price: productData.price,
        productId: productData.productId,
      };
    }
    this.addToBagService.addToCart(this.data).subscribe((res: any) => {
      localStorage.setItem('cartId', res.cartId);
      this.totalCartItems = res.totalCartItems;
      this.addToBagService.cardItemCount(this.totalCartItems);
      localStorage.setItem('totalCount', JSON.stringify(res.totalCartItems));
      this.getCartDetailById();
      // this.addtocartService.cardItemCount(res.totalCartItems);
      this.toastr.success(res.message);
      this.openDialog();
    });
  }
  // End Add To Cart

   // Add To Bag All product On Popup

   getCartDetailById() {
    this.spinner.show();
    let cartId = localStorage.getItem('cartId');
    this.cartService.getCartById(cartId).subscribe(
      res => {
        if (res['status'] == true) {
          this.getResponse = res;
          this.cart = res['data']['cart'];
          this.cartItems = res['data']['cartItems'];
          this.spinner.hide();
          // this.toastr.success(res['message']);
          this.addToBagService.addToBagItemCount(this.cartItems);
          this.addToBagService.cartValueCount(this.cart);
        }
        else {
          this.spinner.hide();
          // this.toastr.error(res['message']);
        }

      }
    )

  }

  // material Add to bag dialog
  openDialog() {
    let config: MatDialogConfig<any> = {
      panelClass: 'myDialogClass'
    }
    const dialogRef = this.dialog.open(AddToBagPopupComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
