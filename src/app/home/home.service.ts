import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  public baseUrl = environment.baseUrl;
  public baseImageUrl = environment.baseImageUrl;

  constructor(private http:HttpClient) { }

  getRecommendedProductList() {
    return this.http.get(`${this.baseUrl}Products/GetRecommendedProductList`);
  }
}
