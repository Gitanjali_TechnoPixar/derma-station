import { Component, OnInit, TemplateRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CartService } from 'src/app/cart/cart.service';
import { AddToBagPopupComponent } from 'src/app/shared/components/add-to-bag-popup/add-to-bag-popup.component';
import { ProductDetailService, MenuService, LanguageService } from 'src/app/shared/services';
import { AddToBagService } from 'src/app/shared/services/add-to-bag.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-ingredient-detail',
  templateUrl: './ingredient-detail.component.html',
  styleUrls: ['./ingredient-detail.component.css'],
})
export class IngredientDetailComponent implements OnInit {
  review: any;
  ingredientProductDetails: any;
  cartValue: any = 0;
  baseImageUrl = environment.baseImageUrl;
  myThumbnail: string;
  modalRef: BsModalRef;
  quantityValueForcart: any = 1;
  currentLang: any;
  totalCartItems: any;
  cartId: any;
  cartItems: any;
  cart: any;
  subTotal: any;
  cartTotal: any;
  cartTotalAmmount: any;
  data = {};
  getResponse: any;
  shippingTotal: any;
  totalDiscounts: any;
  quantityValue: Array<Object> = [
    { value: 1 },
    { value: 2 },
    { value: 3 },
    { value: 4 },
    { value: 5 },
  ];

  constructor(
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private productDetailService: ProductDetailService,
    private menuService: MenuService,
    private modalService: BsModalService,
    private addToBagService: AddToBagService,
    private languageService: LanguageService,
    private cartService: CartService,
    private toastr: ToastrService,
    private dialog: MatDialog

  ) {

    // for translator
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    // for ingradient
    this.route.params.subscribe((res: any) => {
      {
        this.spinner.show();
        this.productDetailService
          .getProductInfoById(atob(res['id']))
          .subscribe((res: any) => {
            if (res.status) {
              this.ingredientProductDetails = res.data;
              this.review = res.data.review;
              this.myThumbnail =
                this.baseImageUrl + this.ingredientProductDetails.imagePaths[0];
              this.spinner.hide();

            } else {
              this.spinner.hide();
              this.menuService.sharedData(null);
            }
          });
      }
    });
  }

  ngOnInit(): void {
    this.getCartDetailById();
  }

  // Select Quantity or Item 
  onSelectQuantity(value: any) {
    this.quantityValueForcart = value;
  }

  getCartDetailById() {
    this.spinner.show();
    let cartId = localStorage.getItem('cartId');
    this.cartService.getCartById(cartId).subscribe(
      res => {
        if (res['status'] == true) {
          this.getResponse = res;
          this.cart = res['data']['cart'];
          this.cartItems = res['data']['cartItems'];
          this.spinner.hide();
          // this.toastr.success(res['message']);
          this.addToBagService.addToBagItemCount(this.cartItems);
          this.addToBagService.cartValueCount(this.cart);
        }
        else {
          this.spinner.hide();
          // this.toastr.error(res['message']);
        }

      }
    )

  }

  // Add to Cart
  openAddToCart(productData: any) {
    // console.log(productData);
    this.cartId = localStorage.getItem('cartId');
    if (this.cartId == null) {
      this.data = {
        cartId: 0,
        quantity: this.quantityValueForcart,
        price: productData.price,
        productId: productData.productId,
      };
    }
    else {
      this.data = {
        // cartId: 0,
        cartId: localStorage.getItem('cartId'),
        quantity: this.quantityValueForcart,
        price: productData.price,
        productId: productData.productId,
      };
    }
    this.addToBagService.addToCart(this.data).subscribe((res: any) => {
      localStorage.setItem('cartId', res.cartId);
      this.totalCartItems = res.totalCartItems;
      this.addToBagService.cardItemCount(this.totalCartItems);
      localStorage.setItem('totalCount', JSON.stringify(res.totalCartItems));
      // this.addtocartService.cardItemCount(res.totalCartItems);
      this.getCartDetailById();
      this.toastr.success(res.message);
      this.openDialog();
    });
  }

  // material dialog
  openDialog() {
    let config: MatDialogConfig<any> = {
      panelClass: 'myDialogClass'
    }
    const dialogRef = this.dialog.open(AddToBagPopupComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
