import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MenuService, LanguageService } from 'src/app/shared/services';
import { ProductService } from '../../shared/services/product.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-ingredients-product-list',
  templateUrl: './ingredients-product-list.component.html',
  styleUrls: ['./ingredients-product-list.component.css'],
})
export class IngredientsProductListComponent implements OnInit {
  ingredientsData: any;
  currentLang: any;
  rate: any;
  baseImageUrl = environment.baseImageUrl;
  subscription: Subscription;
  titleValue: any;
  isShownodata: boolean = false;
  inputValue: any;
  filterresData: any;
  filter = [
    { SortBy: 'IsBestSeller', SortOrder: 'desc', name: 'Best Sellers' },
    { SortBy: 'CreatedOn', SortOrder: 'desc', name: 'New' },
    { SortBy: 'Price', SortOrder: 'asc', name: 'Price Low to High' },
    { SortBy: 'Price', SortOrder: 'desc', name: 'Price High to Low' },
    { SortBy: 'Rating', SortOrder: 'desc', name: 'Top Rated' },
  ]

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private languageService: LanguageService,
    private productService: ProductService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    if (this.router.url == '/ingredient/ingredients-product-list') {
      this.router.navigateByUrl('ingredient/all-ingredients');
    }
  }

  ngOnInit(): void {
    this.spinner.show();
    this.route.params.subscribe((params) => {
      this.inputValue = atob(params['id']);
      var productData = {
        pageNumber: 1,
        pageSize: 10,
        inputValue: atob(params['id']),
        sortBy: '',
        sortOrder: '',
      };
      // Use for future to send id console.log(params['id']);
      this.productService
        .getProductDetailsForIngredient(productData)
        .subscribe((res: any) => {
          if (res.status === false) {
            this.isShownodata = true;
            this.spinner.hide();
          } else {
            this.isShownodata = false;
            this.ingredientsData = res.data.dataList;
            this.titleValue = res.data.dataList;
            // this.rate = res.data.dataList.rating;
            this.spinner.hide();
            if (res.message == "Data shown successfully") {
              this.toastr.success(res.message);
            }
            else {
              this.toastr.error(res.message);
            }
          }
        });
    });
  }

  // for filter Start

  onChangeFilter(evt) {
    let typeVal: string = evt.target.value;
    switch (typeVal) {
      case "Best Sellers":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "IsBestSeller",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForIngredient(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.ingredientsData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });

        break;
      case "New":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "CreatedOn",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForIngredient(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.ingredientsData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price Low to High":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "Price",
          sortOrder: "asc",
        };
        this.productService
          .getProductDetailsForIngredient(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.ingredientsData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price High to Low":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "Price",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForIngredient(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.ingredientsData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Top Rated":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "Rating",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForIngredient(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];
              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.ingredientsData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
    }

  }
  // filter End
  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView();
    }, 500);
    this.router.navigate([
      '/ingredient/ingredients-detail',
      btoa(data.productId),
    ]);
  }
  ngAfterViewInit() {
    window.scrollTo(0, 0);
  }

    // Pagingnation
    totalLength:any
    page: 1;
    goTotop() {
      setTimeout(function () {
        var elmnt = document.getElementById('top');
        elmnt.scrollIntoView({
          behavior: 'smooth',
        });
      }, 0);
    }
}
