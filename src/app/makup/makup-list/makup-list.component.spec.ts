import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakupListComponent } from './makup-list.component';

describe('MakupListComponent', () => {
  let component: MakupListComponent;
  let fixture: ComponentFixture<MakupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
