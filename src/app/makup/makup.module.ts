import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { MakupRoutingModule } from './makup-routing.module';
import { AllMakupComponent } from './all-makup/all-makup.component';
import { MakupListComponent } from './makup-list/makup-list.component';


@NgModule({
  declarations: [AllMakupComponent, MakupListComponent],
  imports: [
    CommonModule,
    MakupRoutingModule,
    SharedModule
  ]
})
export class MakupModule { }
