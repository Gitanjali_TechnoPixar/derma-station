import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllMakupComponent } from './all-makup/all-makup.component';


const routes: Routes = [
  {path:'all-makeup', component:AllMakupComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MakupRoutingModule { }
