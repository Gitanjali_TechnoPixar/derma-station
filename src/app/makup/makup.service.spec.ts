import { TestBed } from '@angular/core/testing';

import { MakupService } from './makup.service';

describe('MakupService', () => {
  let service: MakupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MakupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
