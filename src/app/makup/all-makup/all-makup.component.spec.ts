import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMakupComponent } from './all-makup.component';

describe('AllMakupComponent', () => {
  let component: AllMakupComponent;
  let fixture: ComponentFixture<AllMakupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllMakupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMakupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
