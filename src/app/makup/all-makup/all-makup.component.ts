import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CartService } from 'src/app/cart/cart.service';
import { AddToBagPopupComponent } from 'src/app/shared/components/add-to-bag-popup/add-to-bag-popup.component';
import { AddToBagService, LanguageService } from 'src/app/shared/services';
import { environment } from 'src/environments/environment.prod';
import { MakeupService } from '../makeup.service';

@Component({
  selector: 'app-all-makup',
  templateUrl: './all-makup.component.html',
  styleUrls: ['./all-makup.component.css']
})
export class AllMakupComponent implements OnInit {
  public baseImageUrl = environment.baseImageUrl;
  allProductList:any;
  dataList:any;
  isShownodata: boolean = false;
  currentLang:any;
  makeupList:any;
  cartId:any;
  data = {};
  quantityValueForcart: number = 1;
  totalCartItems:any;
  getResponse:any;
  cartItems:any;
  cart:any;
  
  constructor(
    public translate: TranslateService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private toastr:ToastrService,
    private addToBagService: AddToBagService,
    private cartService: CartService,
    private dialog:MatDialog,
    private makeupService:MakeupService
  ) { 
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit(): void {
    this.getAllMakeuoCategory();
  }

  getAllMakeuoCategory() {
    this.spinner.show();
    let data = {
      pageNumber: 1,
      pageSize: 10,
      inputValue: 3,
    }
    this.makeupService.getProductListForCategory(data).subscribe(
      res => {
        if(res['status']==true){
          this.makeupList = res['data']['dataList']
          this.spinner.hide();
          this.toastr.success(res['message']);
        }
        else{
          this.spinner.hide();
          this.toastr.error(res['message']);

        }
      })
  }

    // Add to Cart
  // openAddToCart(template: TemplateRef<any>, productData: any) {

    openAddToCart(productData: any) {

      // console.log(productData);
      this.cartId = localStorage.getItem('cartId');
      if (this.cartId == null) {
        this.data = {
          cartId: 0,
          quantity: this.quantityValueForcart,
          price: productData.price,
          productId: productData.productId,
        };
      }
      else {
        this.data = {
          // cartId: 0,
          cartId: localStorage.getItem('cartId'),
          quantity: this.quantityValueForcart,
          price: productData.price,
          productId: productData.productId,
        };
      }
      this.addToBagService.addToCart(this.data).subscribe((res: any) => {
        localStorage.setItem('cartId', res.cartId);
        this.totalCartItems = res.totalCartItems;
        this.addToBagService.cardItemCount(this.totalCartItems);
        localStorage.setItem('totalCount', JSON.stringify(res.totalCartItems));
        // this.addtocartService.cardItemCount(res.totalCartItems);
        this.getCartDetailById();
        this.toastr.success(res.message);
        this.openDialog();
      });
    }
  
    // End Add To Cart
  
    // Add To Bag All product On Popup
  
    getCartDetailById() {
      this.spinner.show();
      let cartId = localStorage.getItem('cartId');
      this.cartService.getCartById(cartId).subscribe(
        res => {
  
          if (res['status'] == true) {
            this.getResponse = res;
            this.cart = res['data']['cart'];
            this.cartItems = res['data']['cartItems'];
            this.spinner.hide();
            // this.toastr.success(res['message']);
            this.addToBagService.addToBagItemCount(this.cartItems);
            this.addToBagService.cartValueCount(this.cart);
          }
          else {
            this.spinner.hide();
            // this.toastr.error(res['message']);
          }
  
        }
      )
  
    }
  
    // material dialog
    openDialog() {
      let config : MatDialogConfig<any> = {
        panelClass:'myDialogClass'
      }
      const dialogRef = this.dialog.open(AddToBagPopupComponent, config);
  
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
      });
    }
}
