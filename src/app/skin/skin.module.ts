import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkinRoutingModule } from './skin-routing.module';
import { SkinComponent } from './skin.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material.module';

import {
  AllSkintypeComponent,
  SkintypeProductListComponent,
  SkintypeDetailComponent,
} from '.';

@NgModule({
  declarations: [
    SkinComponent,
    AllSkintypeComponent,
    SkintypeProductListComponent,
    SkintypeDetailComponent,
  ],
  imports: [CommonModule, SkinRoutingModule, SharedModule, MaterialModule],
})
export class SkinModule {}
