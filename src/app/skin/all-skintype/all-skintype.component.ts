import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CartService } from 'src/app/cart/cart.service';
import { AddToBagPopupComponent } from 'src/app/shared/components/add-to-bag-popup/add-to-bag-popup.component';
import { AddToBagService, LanguageService, MenuService } from 'src/app/shared/services';
import { environment } from 'src/environments/environment.prod';
import { SkinService } from '../skin.service';

@Component({
  selector: 'app-all-skintype',
  templateUrl: './all-skintype.component.html',
  styleUrls: ['./all-skintype.component.css'],
})
export class AllSkintypeComponent implements OnInit {
  baseImageUrl = environment.baseImageUrl

  allSkinTypeList: any;
  currentLang: any;
  skinTypeList:any;
  cartId:any;
  data = {};
  quantityValueForcart: number = 1;
  totalCartItems: any;
  getResponse:any;
  cartItems:any;
  cart:any;

  max: number = 5;
  constructor(
    private menuService: MenuService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private router: Router,
    private skinService: SkinService,
    private toastr:ToastrService,
    private dialog:MatDialog,
    private cartService: CartService,
    private addToBagService: AddToBagService,


  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit(): void {
    this.getAllSkinTypes();
    this.getAllSkinCategory();
  }

  getAllSkinTypes() {
    this.spinner.show();
    this.menuService.getAllSkinTypes().subscribe((res: any) => {
      const sorted = res.data.sort((a, b) =>
        a.skinTypeName > b.skinTypeName ? 1 : -1
      );
      const grouped = sorted.reduce((groups, contact) => {
        const letter = contact.skinTypeName.charAt(0);
        groups[letter] = groups[letter] || [];
        groups[letter].push(contact);
        return groups;
      }, {});

      const result = Object.keys(grouped).map((key) => ({
        key,
        contacts: grouped[key],
      }));
      this.allSkinTypeList = result;
      this.spinner.hide();
    });
  }
  gotoDiv(content: any) {
    document.getElementById(content).scrollIntoView({ behavior: 'smooth' });
  }

  getskinTypeList(data: any) {
    this.menuService.sharedData(data);
    localStorage.setItem('sourceData', JSON.stringify(data));
    this.router.navigate(['skin/skintype-product-list', data.skinTypeId]);
  }

  getAllSkinCategory() {
    this.spinner.show();
    let data = {
      pageNumber: 1,
      pageSize: 10,
      inputValue: 1,
    }
    this.skinService.getProductListForCategory(data).subscribe(
      res => {
        if(res['status']==true){
          this.skinTypeList = res['data']['dataList']
          this.spinner.hide();
          this.toastr.success(res['message']);
        }
        else{
          this.spinner.hide();
          this.toastr.error(res['message']);

        }
      })
  }

    // Add to Cart
  // openAddToCart(template: TemplateRef<any>, productData: any) {

    openAddToCart(productData: any) {
      // this.modalRef = this.modalService.show(template);
      // console.log(productData);
      this.cartId = localStorage.getItem('cartId');
      if (this.cartId == null) {
        this.data = {
          cartId: 0,
          quantity: this.quantityValueForcart,
          price: productData.price,
          productId: productData.productId,
        };
      }
      else {
        this.data = {
          // cartId: 0,
          cartId: localStorage.getItem('cartId'),
          quantity: this.quantityValueForcart,
          price: productData.price,
          productId: productData.productId,
        };
      }
      this.addToBagService.addToCart(this.data).subscribe((res: any) => {
        localStorage.setItem('cartId', res.cartId);
        this.totalCartItems = res.totalCartItems;
        this.addToBagService.cardItemCount(this.totalCartItems);
        localStorage.setItem('totalCount', JSON.stringify(res.totalCartItems));
        // this.addtocartService.cardItemCount(res.totalCartItems);
        this.getCartDetailById();
        this.toastr.success(res.message);
        this.openDialog();
      });
    }
  
    // End Add To Cart
  
    // Add To Bag All product On Popup
  
    getCartDetailById() {
      this.spinner.show();
      let cartId = localStorage.getItem('cartId');
      this.cartService.getCartById(cartId).subscribe(
        res => {  
          if (res['status'] == true) {
            this.getResponse = res;
            this.cart = res['data']['cart'];
            this.cartItems = res['data']['cartItems'];
            this.spinner.hide();
            // this.toastr.success(res['message']);
            this.addToBagService.addToBagItemCount(this.cartItems);
            this.addToBagService.cartValueCount(this.cart);
          }
          else {
            this.spinner.hide();
            // this.toastr.error(res['message']);
          }
  
        }
      )
  
    }
  
    // material dialog
    openDialog() {
      let config : MatDialogConfig<any> = {
        panelClass:'myDialogClass'
      }
      const dialogRef = this.dialog.open(AddToBagPopupComponent, config);
  
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
      });
    }
}

