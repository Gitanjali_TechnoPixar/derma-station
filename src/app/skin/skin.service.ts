import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SkinService {
  public baseUrl = environment.baseUrl;

  constructor(private httpClient:HttpClient) { }


  getProductListForCategory(data){
    return this.httpClient.post(`${this.baseUrl}Products/GetProductListForCategory`, data)
  }
}
