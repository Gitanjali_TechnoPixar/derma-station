import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { LanguageService } from 'src/app/shared/services';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProductService } from '../../shared/services/product.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-skintype-product-list',
  templateUrl: './skintype-product-list.component.html',
  styleUrls: ['./skintype-product-list.component.css'],
})
export class SkintypeProductListComponent implements OnInit {
  baseImageUrl = environment.baseImageUrl;
  skinTypeData: any;
  skinData: any;
  currentLang: any;
  titleValue: any;
  rate: any;
  isShowData: boolean = false;
  subscription: Subscription;
  isShownodata: boolean = false;
  inputValue: any;
  filterresData: any;
  sortBy: any;

  filter = [
    { SortBy: 'IsBestSeller', SortOrder: 'desc', name: 'Best Sellers' },
    { SortBy: 'CreatedOn', SortOrder: 'desc', name: 'New' },
    { SortBy: 'Price', SortOrder: 'asc', name: 'Price Low to High' },
    { SortBy: 'Price', SortOrder: 'desc', name: 'Price High to Low' },
    { SortBy: 'Rating', SortOrder: 'desc', name: 'Top Rated' },
  ]

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toaster: ToastrService,
    private languageService: LanguageService,
    private spinner: NgxSpinnerService,
    private productService: ProductService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    if (this.router.url == '/skin/skintype-product-list') {
      this.router.navigateByUrl('skin/all-skintypes');
    }
  }

  ngOnInit(): void {
    this.spinner.show();
    this.route.params.subscribe((params) => {
      var productData = {
        pageNumber: 1,
        pageSize: 10,
        inputValue: atob(params['id']),
        sortBy: '',
        sortOrder: '',
      };
      this.inputValue = atob(params['id']);
      // Use for future to send id console.log(params['id']);
      this.productService
        .getProductDetailsForSkinType(productData)
        .subscribe((res: any) => {
          if (res.status === false) {
            this.spinner.hide();
            this.toaster.error(res.message);
            this.isShownodata = true;
          } else {
            this.isShownodata = false;
            this.skinTypeData = res.data.dataList;
            this.titleValue = res.data.dataList;
            // this.rate = res.data.dataList.rating;
            this.spinner.hide();
            this.toaster.success(res.message);

          }
        });
    });
  }

  // for filter Start


  onChangeFilter(evt) {
    let typeVal: string = evt.target.value;
    switch (typeVal) {
      case "Best Sellers":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "IsBestSeller",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForSkinType(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toaster.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.skinTypeData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toaster.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toaster.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });

        break;
      case "New":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "CreatedOn",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForSkinType(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toaster.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.skinTypeData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toaster.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toaster.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price Low to High":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "Price",
          sortOrder: "asc",
        };
        this.productService
          .getProductDetailsForSkinType(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toaster.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.skinTypeData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toaster.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toaster.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price High to Low":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "Price",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForSkinType(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toaster.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.skinTypeData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toaster.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toaster.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Top Rated":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "Rating",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForSkinType(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toaster.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.skinTypeData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toaster.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toaster.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
    }

  }
  // filter End
  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView();
    }, 500);
    this.router.navigate(['/skin/skintype-detail/', btoa(data.productId)]);
  }

  ngAfterViewInit() {
    window.scrollTo(0, 0);
  }

  // Pagingnation
  totalLength:any;
  page: 1;
  goTotop() {
    setTimeout(function () {
      var elmnt = document.getElementById('top');
      elmnt.scrollIntoView({
        behavior: 'smooth',
      });
    }, 0);
  }
}

