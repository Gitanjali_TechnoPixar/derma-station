import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { HairCareRoutingModule } from './hair-care-routing.module';
import { AllHairCareComponent } from './components/all-hair-care/all-hair-care.component';
import { HairCareListComponent } from './components/hair-care-list/hair-care-list.component';


@NgModule({
  declarations: [AllHairCareComponent, HairCareListComponent],
  imports: [
    CommonModule,
    HairCareRoutingModule,
    SharedModule
  ]
})
export class HairCareModule { }
