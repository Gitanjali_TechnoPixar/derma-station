import { TestBed } from '@angular/core/testing';

import { HaircareService } from './haircare.service';

describe('HaircareService', () => {
  let service: HaircareService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HaircareService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
