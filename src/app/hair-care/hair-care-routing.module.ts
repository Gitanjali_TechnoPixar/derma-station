import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllHairCareComponent } from './components/all-hair-care/all-hair-care.component';


const routes: Routes = [
  {path:'all-hair-care', component:AllHairCareComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HairCareRoutingModule { }
