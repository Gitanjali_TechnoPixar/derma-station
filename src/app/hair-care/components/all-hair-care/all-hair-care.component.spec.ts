import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllHairCareComponent } from './all-hair-care.component';

describe('AllHairCareComponent', () => {
  let component: AllHairCareComponent;
  let fixture: ComponentFixture<AllHairCareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllHairCareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllHairCareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
