import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HairCareListComponent } from './hair-care-list.component';

describe('HairCareListComponent', () => {
  let component: HairCareListComponent;
  let fixture: ComponentFixture<HairCareListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HairCareListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HairCareListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
