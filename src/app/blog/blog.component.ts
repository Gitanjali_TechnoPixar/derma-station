import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { Filters } from '../shared/modals/filter';
import { BlogService } from '../shared/services';
import { LanguageService } from '../shared/services/language.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
})
export class BlogComponent implements OnInit {
  filters: Filters = {
    sortOrder: '',
    sortField: '',
    pageNumber: 1,
    pageSize: 10,
    productCategoryId: 1,
    searchQuery: '',
    filterBy: '',
  };
  currentLang: any = 'en';
  page: 1;
  blogsData: any;
  baseImageUrl = environment.baseImageUrl;
  constructor(
    private blogService: BlogService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private languageService: LanguageService,
    private toastr:ToastrService
  ) {}

  ngOnInit() {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    this.getAllBlogs();
  }

  getAllBlogs() {
    this.spinner.show();
    this.blogService.getAllBlogs(this.filters).subscribe((res: any) => {
      if(res.status === false){
        this.spinner.hide();
        this.toastr.error(res.message);
      }
      else{
        console.log(res);
        this.blogsData = res.data.dataList;
        this.spinner.hide();
        this.toastr.success(res.message);
      }

    });
  }

  getSkinCareList() {
    // console.log(data)
    let data = 1;
    this.spinner.show();
    // localStorage.removeItem('hairCareId');
    localStorage.setItem('skinCareId', JSON.stringify(data));
    this.router.navigate(['blog/blog-list', data]);
    // this.router.navigate(['blog/blog-list']);

  }
  getHairCareList() {
    // console.log(data)
    let data = 2;
    this.spinner.show();
    // localStorage.removeItem('hairCareId');
    localStorage.setItem('skinCareId', JSON.stringify(data));
    this.router.navigate(['blog/blog-list', data]);
    // this.router.navigate(['blog/blog-list']);

  }

  getHairCareBlogs() {
    this.page = 1;
    this.filters.productCategoryId = 2;
    this.spinner.show();
    this.blogService
      .getBlogListByCategory(this.filters)
      .subscribe((res: any) => {
        this.blogsData = res.data.dataList;
        this.spinner.hide();
      });
  }
// Pagingnation
  totalLength:any;
  goTotop() {
    setTimeout(function () {
      var elmnt = document.getElementById('top');
      elmnt.scrollIntoView({
        behavior: 'smooth',
      });
    }, 0);
  }

  readMore(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 100);
    // this.router.navigate(['/blog/blog-details/', btoa(data.id)]);
    this.router.navigate(['/blog/blog-details/', data.id]);

  }
}
