import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllBlogsComponent } from './all-blogs/all-blogs.component';
import { BlogComponent } from './blog.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { BlogListComponent } from './blog-list/blog-list.component';

const routes: Routes = [
  {
    path: '',
    component: BlogComponent,
  },
  {
    path: 'all-blogs',
    component: AllBlogsComponent,
  },
  {
    path: 'blog-details/:id',
    component: BlogDetailComponent,
  },
  {path:'blog-list/:id', component:BlogListComponent},
  {path:'blog-list', component:BlogListComponent},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlogRoutingModule {}
