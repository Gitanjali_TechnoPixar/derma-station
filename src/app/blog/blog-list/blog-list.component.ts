import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Filters } from 'src/app/shared/modals/filter';
import { BlogService, LanguageService } from 'src/app/shared/services';
import { environment } from 'src/environments/environment.prod';


@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {
  blogsData:any;
  page: 1;
  baseImageUrl = environment.baseImageUrl;

  filters: Filters = {
    sortOrder: '',
    sortField: '',
    pageNumber: 1,
    pageSize: 10,
    productCategoryId: 1,
    searchQuery: '',
    filterBy: '',
  };
  currentLang:any;
  isShownodata:boolean=false;
  constructor(
    private blogService: BlogService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private languageService: LanguageService,
    private route:ActivatedRoute,
    private toastr:ToastrService
  ) { 
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit(): void {
    this.getSkinCareBlogs();
  }

  getSkinCareBlogs() {
    this.route.params.subscribe(res=>{
      localStorage.setItem('skin&HairId',res['id']);
      this.filters.productCategoryId =localStorage.getItem('skin&HairId');
      this.page = 1;
    
      this.spinner.show();
      this.blogService
        .getBlogListByCategory(this.filters)
        .subscribe((res: any) => {
          if (res.status === false) {
            this.isShownodata = true;
            this.spinner.hide();
            this.toastr.error(res.message)
          }
          else{
          this.isShownodata = false;
          this.blogsData = res.data.dataList;
          this.spinner.hide();
          this.toastr.success(res.message)

          }
        });

    })
  }
// Read More
  readMore(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 100);
    this.router.navigate(['/blog/blog-details/', data.id]);
  }

  goTotop() {
    setTimeout(function () {
      var elmnt = document.getElementById('top');
      elmnt.scrollIntoView({
        behavior: 'smooth',
      });
    }, 0);
  }

  getSkinCareList() {
    // console.log(data)
    let data = 1;
    this.spinner.show();
    // localStorage.removeItem('hairCareId');
    localStorage.setItem('skinCareId', JSON.stringify(data));
    this.router.navigate(['blog/blog-list', data]);
    // this.router.navigate(['blog/blog-list']);

  }
  getHairCareList() {
    // console.log(data)
    let data = 2;
    this.spinner.show();
    // localStorage.removeItem('hairCareId');
    localStorage.setItem('skinCareId', JSON.stringify(data));
    this.router.navigate(['blog/blog-list', data]);
    // this.router.navigate(['blog/blog-list']);

  }
}
