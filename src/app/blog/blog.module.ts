import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { SharedModule } from '../shared/shared.module';
import { AllBlogsComponent, BlogDetailComponent } from '.';
import { BlogListComponent } from './blog-list/blog-list.component';

@NgModule({
  declarations: [BlogComponent, AllBlogsComponent, BlogDetailComponent, BlogListComponent],
  imports: [CommonModule, BlogRoutingModule, SharedModule],
})
export class BlogModule {}
