import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {
  public baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) { }

  // get personal care sub category Data
  getProductListForSubCategory(pageNumber, pageSize, inputValue, sortBy, sortOrder) {
    return this.httpClient.get(`${this.baseUrl}Products/GetProductListForSubCategory?pageNumber=${pageNumber}&pageSize=${pageSize}&inputValue=${inputValue}&sortBy=${sortBy}&sortOrder=${sortOrder}`)
  }

  // Get personal care sub sub category data on personal care detail paege

  getProductListForSubSubCategory(pageNumber, pageSize, subSubCategoryId) {
    return this.httpClient.get(`${this.baseUrl}Products/GetProductListForSubSubCategory?pageNumber=${pageNumber}&pageSize=${pageSize}&subSubCategoryId=${subSubCategoryId}`)
  }

  getProductListForCategory(data) {
    return this.httpClient.post(`${this.baseUrl}Products/GetProductListForCategory`, data)
  }


  // Api for SHOP By Concern

  getProductListForConcern(pageNumber, pageSize, concernId, sortBy, sortOrder) {
    return this.httpClient.get(`${this.baseUrl}Products/GetProductListForConcern?pageNumber=${pageNumber}&pageSize=${pageSize}&concernId=${concernId}&sortBy=${sortBy}&sortOrder=${sortOrder}`)
  }

  getAllConcerns(){
    return this.httpClient.get(`${this.baseUrl}Products/GetAllConcerns`)
  }


}
