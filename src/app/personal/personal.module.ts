import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonalComponent } from './personal.component';
import { SharedModule } from '../shared/shared.module';

import { PersonalRoutingModule } from './personal-routing.module';
import { AllPersonalCareComponent } from './components/all-personal-care/all-personal-care.component';
import { PersonalCareDetailComponent } from './components/personal-care-detail/personal-care-detail.component';
import { PersonalCareListComponent } from './components/personal-care-list/personal-care-list.component';
import { ConcernListComponent } from './components/concern-list/concern-list.component';
import { ConcernDetailComponent } from './components/concern-detail/concern-detail.component';
import { AllConcernComponent } from './components/all-concern/all-concern.component';
import { MaterialModule } from '../material.module';
import {NgxPaginationModule} from 'ngx-pagination';


@NgModule({
  declarations: [PersonalComponent, AllPersonalCareComponent, PersonalCareDetailComponent, PersonalCareListComponent, ConcernListComponent, ConcernDetailComponent, AllConcernComponent],
  imports: [
    CommonModule,
    PersonalRoutingModule,
    SharedModule,
    MaterialModule,
    NgxPaginationModule
  ],
  // schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class PersonalModule { }
