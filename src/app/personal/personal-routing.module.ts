import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllConcernComponent } from './components/all-concern/all-concern.component';
import { AllPersonalCareComponent } from './components/all-personal-care/all-personal-care.component';
import { ConcernDetailComponent } from './components/concern-detail/concern-detail.component';
import { ConcernListComponent } from './components/concern-list/concern-list.component';
import { PersonalCareDetailComponent } from './components/personal-care-detail/personal-care-detail.component';
import { PersonalCareListComponent } from './components/personal-care-list/personal-care-list.component';
import { PersonalComponent } from './personal.component';


const routes: Routes = [
  {path:'', component:PersonalComponent},
  {path:'all-personal-care', component:AllPersonalCareComponent},
  {path:'personal-care-list', component:PersonalCareListComponent},
  {path:'personal-care-list/:id', component:PersonalCareListComponent},
  {path:'personal-care-detail/:id', component:PersonalCareDetailComponent},
  {path:'concern-list', component:ConcernListComponent},
  {path:'concern-list/:id', component:ConcernListComponent},
  {path:'concern-detail/:id', component:ConcernDetailComponent},
  {path:'all-concern', component:AllConcernComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalRoutingModule { }
