import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalCareDetailComponent } from './personal-care-detail.component';

describe('PersonalCareDetailComponent', () => {
  let component: PersonalCareDetailComponent;
  let fixture: ComponentFixture<PersonalCareDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalCareDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalCareDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
