import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { LanguageService } from 'src/app/shared/services';
import { environment } from 'src/environments/environment.prod';
import { PersonalService } from '../../personal.service';
import { MenuService } from '../../../shared/services/menu.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddToBagService } from '../../../shared/services/add-to-bag.service';
import { ProductDetailService } from '../../../shared/services/product-detail.service';
import { CartService } from 'src/app/cart/cart.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddToBagPopupComponent } from 'src/app/shared/components/add-to-bag-popup/add-to-bag-popup.component';

@Component({
  selector: 'app-personal-care-detail',
  templateUrl: './personal-care-detail.component.html',
  styleUrls: ['./personal-care-detail.component.css']
})
export class PersonalCareDetailComponent implements OnInit {
  baseImageUrl = environment.baseImageUrl;
  myThumbnail: string;
  quantityValueForcart: any = 1;
  modalRef: BsModalRef;
  totalCartItems: any;
  imagePath: any;
  brandProductDetails: any;
  currentLang: any;
  cartId: any;
  data = {};
  getResponse: any;
  cart: any;
  cartItems: any;
  subTotal: any;
  shippingTotal: any;
  totalDiscounts: any;
  cartTotal: any;
  cartTotalAmmount: any;
  review: any;

  quantityValue: Array<Object> = [
    { value: 1 },
    { value: 2 },
    { value: 3 },
    { value: 4 },
    { value: 5 },
  ];
  constructor(
    public translate: TranslateService,
    private personalService: PersonalService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private route: ActivatedRoute,
    private menuService: MenuService,
    private modalService: BsModalService,
    private addToBagService: AddToBagService,
    private productDetailService: ProductDetailService,
    private cartService: CartService,
    private toastr: ToastrService,
    private dialog: MatDialog
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }


  ngOnInit(): void {
    this.personalCareDetail();
  }

  personalCareDetail() {
    this.route.params.subscribe((res: any) => {
      {
        this.spinner.show();
        this.productDetailService
          .getProductInfoById(atob(res['id'])).subscribe(
            res => {
              if (res['status']) {
                this.brandProductDetails = res['data'];
                this.review = res['data']['review'];
                this.myThumbnail = this.baseImageUrl + this.brandProductDetails.imagePaths[0];
                this.spinner.hide();


              } else {
                this.spinner.hide();
                this.menuService.sharedData(null);
              }
            });
      }
    });

  }

  onSelectQuantity(value: any) {
    this.quantityValueForcart = parseInt(value);
    this.quantityValueForcart = value;
  }

  // Add to Cart
  // openAddToCart(template: TemplateRef<any>, productData: any) {

  openAddToCart(productData: any) {

    // console.log(productData);
    this.cartId = localStorage.getItem('cartId');
    if (this.cartId == null) {
      this.data = {
        cartId: 0,
        quantity: this.quantityValueForcart,
        price: productData.price,
        productId: productData.productId,
      };
    }
    else {
      this.data = {
        // cartId: 0,
        cartId: localStorage.getItem('cartId'),
        quantity: this.quantityValueForcart,
        price: productData.price,
        productId: productData.productId,
      };
    }
    this.addToBagService.addToCart(this.data).subscribe((res: any) => {
      localStorage.setItem('cartId', res.cartId);
      this.totalCartItems = res.totalCartItems;
      this.addToBagService.cardItemCount(this.totalCartItems);
      localStorage.setItem('totalCount', JSON.stringify(res.totalCartItems));
      // this.addtocartService.cardItemCount(res.totalCartItems);
      this.getCartDetailById();
      this.toastr.success(res.message);
      this.openDialog();
    });
  }

  // End Add To Cart

  // Add To Bag All product On Popup

  getCartDetailById() {
    this.spinner.show();
    let cartId = localStorage.getItem('cartId');
    this.cartService.getCartById(cartId).subscribe(
      res => {

        if (res['status'] == true) {
          this.getResponse = res;
          this.cart = res['data']['cart'];
          this.cartItems = res['data']['cartItems'];
          this.spinner.hide();
          // this.toastr.success(res['message']);
          this.addToBagService.addToBagItemCount(this.cartItems);
          this.addToBagService.cartValueCount(this.cart);
        }
        else {
          this.spinner.hide();
          // this.toastr.error(res['message']);
        }

      }
    )

  }

  // material dialog
  openDialog() {
    let config: MatDialogConfig<any> = {
      panelClass: 'myDialogClass'
    }
    const dialogRef = this.dialog.open(AddToBagPopupComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
