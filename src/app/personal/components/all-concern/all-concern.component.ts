import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { LanguageService } from 'src/app/shared/services';
import { PersonalService } from '../../personal.service';

@Component({
  selector: 'app-all-concern',
  templateUrl: './all-concern.component.html',
  styleUrls: ['./all-concern.component.css']
})
export class AllConcernComponent implements OnInit {
  allConcernList: any;
  currentLang: any;

  constructor(
    public translate: TranslateService,
    private personalService: PersonalService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private route: ActivatedRoute,
    private toastr:ToastrService
  ) { 
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  switchLanguage(language: string) {
    this.translate.use(language);
  }

  ngOnInit(): void {
    this.getAllConcern();
  }


  getAllConcern() {
    this.spinner.show();
    this.personalService.getAllConcerns().subscribe((res: any) => {
      const sorted = res.data.sort((a, b) =>
        a.concernName > b.concernName ? 1 : -1
      );
      const grouped = sorted.reduce((groups, contact) => {
        const letter = contact.concernName.charAt(0);
        groups[letter] = groups[letter] || [];
        groups[letter].push(contact);
        return groups;
      }, {});

      const result = Object.keys(grouped).map((key) => ({
        key,
        contacts: grouped[key],
      }));
      this.allConcernList = result;
      this.spinner.hide();
      // this.toastr.success(res['message']);
      if(res['message']=="Data shown successfully"){
        this.toastr.success(res['message']);
      }
      else{
        this.toastr.error(res['message']);
      }

    });
  }

  gotoDiv(content: any) {
    document.getElementById(content).scrollIntoView({ behavior: 'smooth' });
  }

  getconcernDetails(data: any) {
    // this.menuService.sharedData(data);
    localStorage.setItem('concernData', JSON.stringify(data));
    this.router.navigate([
      'personal/personal-care-list',btoa(data.concernId),
    ]);
  }
}
