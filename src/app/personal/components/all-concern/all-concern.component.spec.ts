import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllConcernComponent } from './all-concern.component';

describe('AllConcernComponent', () => {
  let component: AllConcernComponent;
  let fixture: ComponentFixture<AllConcernComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllConcernComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllConcernComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
