import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { LanguageService } from 'src/app/shared/services';
import { environment } from 'src/environments/environment.prod';
import { PersonalService } from '../../personal.service';

@Component({
  selector: 'app-concern-list',
  templateUrl: './concern-list.component.html',
  styleUrls: ['./concern-list.component.css']
})
export class ConcernListComponent implements OnInit {
  public baseImageUrl = environment.baseImageUrl;
  isShownodata: boolean = false;
  resData: any;
  dataList: any;
  rate: number = 5;
  currentLang: any;
  pager = {};
  pageOfItems = [];
  currentPage: any;
  totalPages: any;
  totalCount: any;
  pageSize: any = 10;
  concernId: any;
  totalLength: any;
  page: number = 1;
  id: any;
  filterPageNo: any;
  filterPageSize: any;
  filterresData: any;
  filterDataList: any;
  filtercurrentPage: any;
  filtTotalPages: any;
  filtPageSize: any;
  filterTotalCount: any;
  filter = [
    { SortBy: 'IsBestSeller', SortOrder: 'desc', name: 'Best Sellers' },
    { SortBy: 'CreatedOn', SortOrder: 'desc', name: 'New' },
    { SortBy: 'Price', SortOrder: 'asc', name: 'Price Low to High' },
    { SortBy: 'Price', SortOrder: 'desc', name: 'Price High to Low' },
    { SortBy: 'Rating', SortOrder: 'desc', name: 'Top Rated' },
  ]

  constructor(
    public translate: TranslateService,
    private personalService: PersonalService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit(): void {
    this.personalCareConcernList();

  }

  // Personal Care Concern List
  personalCareConcernList() {
    this.spinner.show();
    this.route.params.subscribe((params) => {
      this.pageNumber = 1;
      this.pageSize = 10;
      let concernId = atob(params['id']);
      this.concernId = concernId;
      let sortBy: string = "";
      let sortOrder: string = "";
      // Use for future to send id console.log(params['id']);
      this.personalService
        .getProductListForConcern(this.pageNumber, this.pageSize, concernId, sortBy, sortOrder).subscribe(
          res => {
            // this.dataList = this.resData['dataList'];

            // if (this.dataList.length === 0) {
            if (res['data'].length === 0) {
              this.isShownodata = true;
              this.spinner.hide();
              this.toastr.error(res['message']);
            } else {
              this.resData = res['data'];
              this.dataList = this.resData['dataList'];
              // this.rate = res['data']['dataList']['rating'];
              this.currentPage = this.resData['currentPage'];
              this.pageSize = this.resData['pageSize'];
              this.totalPages = this.resData['totalPages'];
              this.totalCount = this.resData['totalCount'];
              // this.totalLength=this.dataList.length;
              this.spinner.hide();
              if (res['message'] == "Data shown successfully") {
                this.toastr.success(res['message']);
                this.isShownodata = false;
              }
              else {
                this.toastr.error(res['message']);
                this.isShownodata = true;
              }
            }
          });
    });
  }

  // for filter Start
  pageNumber: any;
  sortBy: any;
  sortOrder: any;
  onChangeFilter(evt) {
    let typeVal: string = evt.target.value;
    switch (typeVal) {
      case "Best Sellers":
        this.filterPageNo = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.id = this.concernId;
        this.sortBy = "IsBestSeller";
        this.sortOrder = "desc";
        this.personalService
          .getProductListForConcern(this.filterPageNo, this.filterPageSize, this.id, this.sortBy, this.sortOrder).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data'];
                this.resData = this.filterresData;
                this.filterDataList = this.resData['dataList'];
                this.dataList = this.filterDataList;
                // this.rate = res['data']['dataList']['rating'];
                this.filtercurrentPage = this.resData['currentPage'];
                this.currentPage = this.filtercurrentPage;
                this.filtPageSize = this.resData['pageSize'];
                this.pageSize = this.filtPageSize;
                this.filtTotalPages = this.resData['totalPages'];
                this.totalPages = this.filtTotalPages
                this.filterTotalCount = this.resData['totalCount'];
                this.totalCount = this.filterTotalCount;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });

        break;
      case "New":
        this.filterPageNo = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.id = this.concernId;
        this.sortBy = "CreatedOn";
        this.sortOrder = "desc";
        this.personalService
          .getProductListForConcern(this.filterPageNo, this.filterPageSize, this.id, this.sortBy, this.sortOrder).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data'];
                this.resData = this.filterresData;
                this.filterDataList = this.resData['dataList'];
                this.dataList = this.filterDataList;
                // this.rate = res['data']['dataList']['rating'];
                this.filtercurrentPage = this.resData['currentPage'];
                this.currentPage = this.filtercurrentPage;
                this.filtPageSize = this.resData['pageSize'];
                this.pageSize = this.filtPageSize;
                this.filtTotalPages = this.resData['totalPages'];
                this.totalPages = this.filtTotalPages
                this.filterTotalCount = this.resData['totalCount'];
                this.totalCount = this.filterTotalCount;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price Low to High":
        this.filterPageNo = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.id = this.concernId;
        this.sortBy = "Price";
        this.sortOrder = "asc";
        this.personalService
          .getProductListForConcern(this.filterPageNo, this.filterPageSize, this.id, this.sortBy, this.sortOrder).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data'];
                this.resData = this.filterresData;
                this.filterDataList = this.resData['dataList'];
                this.dataList = this.filterDataList;
                // this.rate = res['data']['dataList']['rating'];
                this.filtercurrentPage = this.resData['currentPage'];
                this.currentPage = this.filtercurrentPage;
                this.filtPageSize = this.resData['pageSize'];
                this.pageSize = this.filtPageSize;
                this.filtTotalPages = this.resData['totalPages'];
                this.totalPages = this.filtTotalPages
                this.filterTotalCount = this.resData['totalCount'];
                this.totalCount = this.filterTotalCount;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price High to Low":
        this.filterPageNo = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.id = this.concernId;
        this.sortBy = "Price";
        this.sortOrder = "desc";
        this.personalService
          .getProductListForConcern(this.filterPageNo, this.filterPageSize, this.id, this.sortBy, this.sortOrder).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data'];
                this.resData = this.filterresData;
                this.filterDataList = this.resData['dataList'];
                this.dataList = this.filterDataList;
                // this.rate = res['data']['dataList']['rating'];
                this.filtercurrentPage = this.resData['currentPage'];
                this.currentPage = this.filtercurrentPage;
                this.filtPageSize = this.resData['pageSize'];
                this.pageSize = this.filtPageSize;
                this.filtTotalPages = this.resData['totalPages'];
                this.totalPages = this.filtTotalPages
                this.filterTotalCount = this.resData['totalCount'];
                this.totalCount = this.filterTotalCount;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Top Rated":
        this.filterPageNo = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.id = this.concernId;
        this.sortBy = "Rating";
        this.sortOrder = "desc";
        this.personalService
          .getProductListForConcern(this.filterPageNo, this.filterPageSize, this.id, this.sortBy, this.sortOrder).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data'];
                this.resData = this.filterresData;
                this.filterDataList = this.resData['dataList'];
                this.dataList = this.filterDataList;
                // this.rate = res['data']['dataList']['rating'];
                this.filtercurrentPage = this.resData['currentPage'];
                this.currentPage = this.filtercurrentPage;
                this.filtPageSize = this.resData['pageSize'];
                this.pageSize = this.filtPageSize;
                this.filtTotalPages = this.resData['totalPages'];
                this.totalPages = this.filtTotalPages
                this.filterTotalCount = this.resData['totalCount'];
                this.totalCount = this.filterTotalCount;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
    }
  }

  // filter End

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/personal/personal-care-detail/' + btoa(data.productId)]);
  }

      // Pagingnation
      goTotop() {
        setTimeout(function () {
          var elmnt = document.getElementById('top');
          elmnt.scrollIntoView({
            behavior: 'smooth',
          });
        }, 0);
      }
}
