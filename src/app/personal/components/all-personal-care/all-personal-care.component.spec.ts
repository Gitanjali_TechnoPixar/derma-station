import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllPersonalCareComponent } from './all-personal-care.component';

describe('AllPersonalCareComponent', () => {
  let component: AllPersonalCareComponent;
  let fixture: ComponentFixture<AllPersonalCareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllPersonalCareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllPersonalCareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
