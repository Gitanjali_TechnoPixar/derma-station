import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalCareListComponent } from './personal-care-list.component';

describe('PersonalCareListComponent', () => {
  let component: PersonalCareListComponent;
  let fixture: ComponentFixture<PersonalCareListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalCareListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalCareListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
