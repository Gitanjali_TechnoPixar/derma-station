import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { LanguageService } from 'src/app/shared/services';
import { environment } from 'src/environments/environment.prod';
import { PersonalService } from '../../personal.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-personal-care-list',
  templateUrl: './personal-care-list.component.html',
  styleUrls: ['./personal-care-list.component.css']
})
export class PersonalCareListComponent implements OnInit {
  baseImageUrl = environment.baseImageUrl
  isShownodata: boolean = false;
  personalData: any;
  rate: number = 5;
  dataList: any;
  resData: any;
  currentLang: any;
  pageNumber: any;
  pageSize: any;
  inputValue: any;
  id: any;
  filterPageNo: any;
  filterPageSize: any;
  filterresData: any;
  filter = [
    { SortBy: 'IsBestSeller', SortOrder: 'desc', name: 'Best Sellers' },
    { SortBy: 'CreatedOn', SortOrder: 'desc', name: 'New' },
    { SortBy: 'Price', SortOrder: 'asc', name: 'Price Low to High' },
    { SortBy: 'Price', SortOrder: 'desc', name: 'Price High to Low' },
    { SortBy: 'Rating', SortOrder: 'desc', name: 'Top Rated' },
  ]
  constructor(
    public translate: TranslateService,
    private personalService: PersonalService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private route: ActivatedRoute,
    private toastr: ToastrService

  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    this.spinner.show();
  }
  switchLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit(): void {
    this.personalCareCategoryList();
  }

  personalCareCategoryList() {
    this.spinner.show();
    this.route.params.subscribe((params) => {
      this.pageNumber = 1;
      this.pageSize = 10;
      this.inputValue = atob(params['id']);;
      let sortBy: string = "";
      let sortOrder: string = "";
      // Use for future to send id console.log(params['id']);
      this.personalService
        .getProductListForSubCategory(this.pageNumber, this.pageSize, this.inputValue, sortBy, sortOrder).subscribe(
          res => {
            this.resData = res['data'];
            this.dataList = this.resData;
            if (this.dataList.length === 0) {
              this.isShownodata = true;
              this.spinner.hide();
              this.toastr.error(res['message']);
            } else {
              this.isShownodata = false;
              this.dataList = this.resData;
              // this.rate = res['data']['dataList']['rating'];
              this.spinner.hide();
              if (res['message'] == "Data shown successfully") {
                this.toastr.success(res['message']);
                this.isShownodata = false;
              }
              else {
                this.toastr.error(res['message']);
                this.isShownodata = true;
              }

            }
          });
    });
  }

  // for filter Start

  sortBy: any;
  sortOrder: any;
  onChangeFilter(evt) {
    let typeVal: string = evt.target.value;
    switch (typeVal) {
      case "Best Sellers":
        this.filterPageNo = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.id = this.inputValue;
        this.sortBy = "IsBestSeller";
        this.sortOrder = "desc";
        this.personalService
          .getProductListForSubCategory(this.filterPageNo, this.filterPageSize, this.id, this.sortBy, this.sortOrder).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data'];
                this.dataList = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });

        break;
      case "New":
        this.filterPageNo = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.id = this.inputValue;
        this.sortBy = "CreatedOn";
        this.sortOrder = "desc";
        this.personalService
          .getProductListForConcern(this.filterPageNo, this.filterPageSize, this.id, this.sortBy, this.sortOrder).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data'];
                this.dataList = this.filterresData;

                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price Low to High":
        this.filterPageNo = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.id = this.inputValue;
        this.sortBy = "Price";
        this.sortOrder = "asc";
        this.personalService
          .getProductListForConcern(this.filterPageNo, this.filterPageSize, this.id, this.sortBy, this.sortOrder).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data'];
                this.dataList = this.filterresData;

                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price High to Low":
        this.filterPageNo = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.id = this.inputValue;
        this.sortBy = "Price";
        this.sortOrder = "desc";
        this.personalService
          .getProductListForConcern(this.filterPageNo, this.filterPageSize, this.id, this.sortBy, this.sortOrder).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data'];
                this.dataList = this.filterresData;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Top Rated":
        this.filterPageNo = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.id = this.inputValue;
        this.sortBy = "Rating";
        this.sortOrder = "desc";
        this.personalService
          .getProductListForConcern(this.filterPageNo, this.filterPageSize, this.id, this.sortBy, this.sortOrder).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data'];
                this.dataList = this.filterresData;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
    }

  }
  // filter End
  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/personal/personal-care-detail/' + btoa(data.productId)]);
  }

      // Pagingnation
      totalLength: any;
      page: 1;
      goTotop() {
        setTimeout(function () {
          var elmnt = document.getElementById('top');
          elmnt.scrollIntoView({
            behavior: 'smooth',
          });
        }, 0);
      }
}
