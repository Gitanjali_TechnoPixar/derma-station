import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  public baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) { }


  getCartById(cartId){
    return this.httpClient.get(`${this.baseUrl}Orders/GetCartById?cartId=${cartId}`)
  }

  deleteCartItem(cartItemId){
    return this.httpClient.delete(`${this.baseUrl}Orders/DeleteCartItem?cartItemId=${cartItemId}`)
  }

}
