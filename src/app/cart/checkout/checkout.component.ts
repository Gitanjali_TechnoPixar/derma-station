import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AddToBagService, LanguageService, ProductService } from 'src/app/shared/services';
import { environment } from 'src/environments/environment.prod';
import { CartService } from '../cart.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
})
export class CheckoutComponent implements OnInit {
  baseImageUrl = environment.baseImageUrl;
  cart: any;
  cartItems: any;
  getResponse: any;
  subTotal: any;
  cartTotalAmmount: any;
  totalCartItems: any;
  isShownodata: boolean = false;
  brandId:any;
  currentLang: any;


  constructor(
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    private languageService: LanguageService,
    private toastr: ToastrService,
    private cartService: CartService,
    private router: Router,
    private addtocartService: AddToBagService,
    private productService: ProductService,
    public translate: TranslateService,

  ) { 
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    this.spinner.show();
  }

  ngOnInit(): void {
    this.getCartDetailById();
  }


  getCartDetailById() {
    this.spinner.show();
    let cartId = localStorage.getItem('cartId');
    this.cartService.getCartById(cartId).subscribe(
      res => {
        if (res['status'] == true) {
          this.getResponse = res;
          this.cart = res['data']['cart'];
          this.cartItems = res['data']['cartItems'];
          this.subTotal = this.cart['subTotal'];
          this.cartTotalAmmount = this.cart['cartTotal'];
          this.spinner.hide();
          this.toastr.success(res['message']);
        }
        else {
          this.spinner.hide();
          this.toastr.error(res['message']);
        }

      }
    )

  }

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/brand/brands-detail/' + btoa(data.productId)]);
  }


  // Delete product By ItemId
  deleteCartProduct(data) {
    this.spinner.show();
    this.cartService.deleteCartItem(data).subscribe(
      res => {
        if (res['status'] == true) {
          this.spinner.hide();
          this.getResponse;
          this.toastr.success(res['message']);
          setTimeout(() => {
            this.getCartDetailById();
          }, 2000)
          this.openAddToCart();
          // console.log(res);
        }
        else {
          this.spinner.hide();
          this.toastr.error(res['message']);
        }
      }
    )

  }

  //Update Add to Cart after deleted Cart Item
  openAddToCart() {
    var data = {
      cartId: localStorage.getItem('cartId'),
      // cartId: 0,
      quantity: 0,
      price: 0,
      productId: 0,
    };

    this.addtocartService.addToCart(data).subscribe((res: any) => {
      // console.log(res);
      localStorage.setItem('cartId', res.cartId);
      this.totalCartItems = res.totalCartItems;
      this.addtocartService.cardItemCount(this.totalCartItems);
      localStorage.setItem('totalCount', JSON.stringify(res.totalCartItems));

      // this.addtocartService.cardItemCount(res.totalCartItems);
    });
  }

  // countinue Shoping

  countinueshop() {
    this.brandId=localStorage.getItem('countinueShoping')
    // console.log(this.brandId);
    this.router.navigate(['/brand/brands-product-list/', btoa(this.brandId)]);

  }

  // Countinue shoping

  gotoPage(routerUrl: any) {
    this.spinner.show();
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({
        behavior: 'smooth',
      });
    }, 0);
    this.router.navigateByUrl(routerUrl);
    this.spinner.hide();
  }
}
