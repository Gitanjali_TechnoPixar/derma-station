import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CartRoutingModule } from './cart-routing.module';

import { CartComponent, CheckoutComponent } from '.';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [CheckoutComponent, CartComponent],
  imports: [CommonModule, CartRoutingModule, SharedModule,
  ],
})
export class CartModule {}
