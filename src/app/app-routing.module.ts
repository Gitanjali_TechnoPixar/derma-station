import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './shared/components/product/product.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'offers',
    loadChildren: () =>
      import('./offers/offers.module').then((m) => m.OffersModule),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginModule),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./register/register.module').then((m) => m.RegisterModule),
  },
  {
    path: 'brand',
    loadChildren: () =>
      import('./brand/brand.module').then((m) => m.BrandModule),
  },
  {path:'hair-care', 
  loadChildren:() => import('./hair-care/hair-care.module').then((m)=> m.HairCareModule)},
  {
    path: 'concern',
    loadChildren: () =>
      import('./concern/concern.module').then((m) => m.ConcernModule),
  },
  {
    path: 'cart',
    loadChildren: () => import('./cart/cart.module').then((m) => m.CartModule),
  },
  {
    path: 'skin',
    loadChildren: () => import('./skin/skin.module').then((m) => m.SkinModule),
  },
  {
    path: 'ingredient',
    loadChildren: () =>
      import('./ingredient/ingredient.module').then((m) => m.IngredientModule),
  },
  {path:'makeup', loadChildren:() => import('./makup/makup.module').then((m)=> m.MakupModule)},
  {path:'body-bath', loadChildren:() => import('./body/body.module').then((m)=> m.BodyModule)},
  {path:'pro-collagen', loadChildren:() => import('./pro-collagen/pro-collagen.module').then((m)=> m.ProCollagenModule)},


  {
    path: 'consult-doctor',
    loadChildren: () =>
      import('./consult-doctor/consult-doctor.module').then(
        (m) => m.ConsultDoctorModule
      ),
  },
  {
    path: 'blog',
    loadChildren: () => import('./blog/blog.module').then((m) => m.BlogModule),
  },
  {path:'personal', loadChildren:()=>import('./personal/personal.module').then(m=>m.PersonalModule)},
  { path: 'products', component: ProductComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
