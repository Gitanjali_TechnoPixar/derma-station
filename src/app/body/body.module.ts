import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BodyRoutingModule } from './body-routing.module';
import { BodyBathListComponent } from './body-bath-list/body-bath-list.component';
import { AllBodyBathComponent } from './all-body-bath/all-body-bath.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [BodyBathListComponent, AllBodyBathComponent],
  imports: [
    CommonModule,
    BodyRoutingModule,
    SharedModule,

  ]
})
export class BodyModule { }
