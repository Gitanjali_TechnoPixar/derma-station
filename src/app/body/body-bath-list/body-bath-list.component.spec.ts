import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyBathListComponent } from './body-bath-list.component';

describe('BodyBathListComponent', () => {
  let component: BodyBathListComponent;
  let fixture: ComponentFixture<BodyBathListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyBathListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyBathListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
