import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllBodyBathComponent } from './all-body-bath.component';

describe('AllBodyBathComponent', () => {
  let component: AllBodyBathComponent;
  let fixture: ComponentFixture<AllBodyBathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllBodyBathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllBodyBathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
