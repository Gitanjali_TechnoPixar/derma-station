import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllBodyBathComponent } from './all-body-bath/all-body-bath.component';


const routes: Routes = [
  {path:'shop-all-body-bath', component:AllBodyBathComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BodyRoutingModule { }
