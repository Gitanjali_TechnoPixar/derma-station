import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BodyService {
  baseUrl = environment.baseUrl;
  constructor(private httpClient:HttpClient) { }

  getProductListForCategory(data) {
    return this.httpClient.post(`${this.baseUrl}Products/GetProductListForCategory`, data)
  }
}
