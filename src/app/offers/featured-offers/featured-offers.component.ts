import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { LanguageService } from 'src/app/shared/services';
import { environment } from 'src/environments/environment.prod';
import { OffersService } from '../offers.service';

@Component({
  selector: 'app-featured-offers',
  templateUrl: './featured-offers.component.html',
  styleUrls: ['./featured-offers.component.css'],
})
export class FeaturedOffersComponent implements OnInit {
  currentLang:any;
  offerListResponse:any;
  isShownodata: boolean = false;
  baseImageUrl = environment.baseImageUrl



  constructor(
    private offersService:OffersService,
    public translate: TranslateService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private route: ActivatedRoute,
    private toastr:ToastrService
    ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit(): void {
    this.featuredOffersList();
  }

  featuredOffersList(){
    this.spinner.show();
    this.offersService.getFeaturedOffersList().subscribe(
      res=>{
        // console.log(res);
        if(res['status']==false){
          this.isShownodata = true;
          this.spinner.hide();
        }
        else{
          this.spinner.show();
          if(res['status']==true){
            this.offerListResponse=res['data'];
            this.toastr.success(res['message']);
            this.spinner.hide();
            this.isShownodata = false;

          }
          else{
            this.toastr.error(res['message']);
            // this.isShownodata = true;
            this.spinner.hide();

          }

        }
        // console.log(this.offerListResponse);
      }
    )
  }

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/personal/personal-care-detail/' + btoa(data.productId)]);
  }

    // Pagingnation
    page: 1;
    totalLength:any;
    goTotop() {
      setTimeout(function () {
        var elmnt = document.getElementById('top');
        elmnt.scrollIntoView({
          behavior: 'smooth',
        });
      }, 0);
    }

}
