import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OffersService {
  public baseUrl = environment.baseUrl;

  constructor(private httpClient:HttpClient) { }


  getFeaturedOffersList(){
    return this.httpClient.get(`${this.baseUrl}Products/GetFeaturedOffersList`)
  }

}
