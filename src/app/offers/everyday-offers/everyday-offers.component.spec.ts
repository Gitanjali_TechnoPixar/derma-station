import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EverydayOffersComponent } from './everyday-offers.component';

describe('EverydayOffersComponent', () => {
  let component: EverydayOffersComponent;
  let fixture: ComponentFixture<EverydayOffersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EverydayOffersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EverydayOffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
