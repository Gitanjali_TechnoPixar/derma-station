import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { RegisterService } from './register.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm:FormGroup;
  submitted = false;
  response:any;
  accessToken:any;
  dialCode:any;
  input:boolean=false;
  constructor( private fb:FormBuilder,
    private spinner: NgxSpinnerService,
    private registerService:RegisterService,
    private router: Router,
    private toastr: ToastrService,

    ) { }
  
  ngOnInit(): void {
    this.regForms();
  }
 
  regForms(){
    this.registerForm = this.fb.group({
      email: new FormControl('',[Validators.required, Validators.email]),
      dialCode: new FormControl(this.dialCode),
      phoneNumber: new FormControl('',[Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]),
      password: new FormControl('',[Validators.required, Validators.minLength(6)])
    })
  }

  get getFormControl() {
    return this.registerForm['controls'];
  }

  onSubmit(form) {
    const formValues = this.registerForm.value;
    const formData = {};
    formValues['dialCode']=this.dialCode;
    formData['email'] = formValues['email'];
    formData['dialCode'] = formValues['dialCode'];
    formData['phoneNumber'] = formValues['phoneNumber'];
    formData['password'] = formValues['password'];

    this.spinner.show();
    this.submitted = true;
    if (this.registerForm.invalid) {
      this.spinner.hide();
      this.input=true;
      // this.toastr.error('Invalid Form, Kindaly Fill The Form');
      return;
    } 
    else {
      this.input=false;
      this.registerService.postRegister(formData).subscribe(
        (res: any) => {
          this.response=res;
          if (this.response['status'] == true) {
            this.spinner.hide();
            this.accessToken = this.response['data']['accessToken'];
            localStorage.setItem("token",this.accessToken);

            this.toastr.success(res.message);
            this.router.navigateByUrl('/login');
          } 
          else if(this.response['status'] == false){
            this.spinner.hide();
            this.toastr.error(res.message);
          }
          else{
            this.spinner.hide();
            this.toastr.error(res.message);
          }
        });
    }
  }


  telInputObject(obj) {
    // obj.setCountry('in');
    this.dialCode = obj.s.dialCode;
  }
  getNumber(obj) {
    console.log(obj);
    // obj.setCountry('in');
  }
  onCountryChange(obj) {
    console.log('change event object',obj);
    // obj.name('in');
    this.dialCode = obj.dialCode;
    // console.log('this is dial code', this.dialCode);
  }

  // signInHasError(status: any) {
  //   console.log('Error msg', status)
  //   if(status == null) {
  //     this.submitted = false
  //     this.registerForm.value.phoneNumber.setValue(null)
  //   } else {
  //     this.submitted = status;
  //   }      
  // };

  // (hasError)="signInHasError($event)" use on signInHasError method
}
