import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  public baseUrl = environment.baseUrl


  constructor(private http:HttpClient) { }

  postRegister(data:any){
    return this.http.post(`${this.baseUrl}Auth/Register`, data)
  }
}
