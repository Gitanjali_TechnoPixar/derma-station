import { TestBed } from '@angular/core/testing';

import { ProCollagenService } from './pro-collagen.service';

describe('ProCollagenService', () => {
  let service: ProCollagenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProCollagenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
