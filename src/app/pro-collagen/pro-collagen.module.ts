import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { ProCollagenRoutingModule } from './pro-collagen-routing.module';
import { AllProCollagenComponent } from './all-pro-collagen/all-pro-collagen.component';


@NgModule({
  declarations: [AllProCollagenComponent],
  imports: [
    CommonModule,
    ProCollagenRoutingModule,
    SharedModule
  ]
})
export class ProCollagenModule { }
