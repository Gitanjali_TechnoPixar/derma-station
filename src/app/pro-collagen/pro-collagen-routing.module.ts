import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllProCollagenComponent } from './all-pro-collagen/all-pro-collagen.component';


const routes: Routes = [
  {path:'shop-all-pro-collagen', component:AllProCollagenComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProCollagenRoutingModule { }
