import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllProCollagenComponent } from './all-pro-collagen.component';

describe('AllProCollagenComponent', () => {
  let component: AllProCollagenComponent;
  let fixture: ComponentFixture<AllProCollagenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllProCollagenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllProCollagenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
