import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandComponent } from './brand.component';
import { AllBrandsComponent } from './components/all-brands/all-brands.component';
import { BrandProductListComponent } from './components/brand-product-list/brand-product-list.component';
import { BrandDetailComponent } from './components/brand-detail/brand-detail.component';

const routes: Routes = [
  {
    path: '',
    component: BrandComponent,
  },
  {
    path: 'all-brands',
    component: AllBrandsComponent,
  },
  {
    path: 'brands-product-list',
    component: BrandProductListComponent,
  },
  { path: 'brands-product-list/:id', component: BrandProductListComponent },
  {
    path: 'brands-detail/:id',
    component: BrandDetailComponent,
  },
  {
    path: '**',
    component: AllBrandsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrandRoutingModule {}
