import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MenuService } from '../../../shared/services/menu.service';
import { ProductDetailService } from '../../../shared/services/product-detail.service';
import { environment } from 'src/environments/environment.prod';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddToBagService } from '../../../shared/services/add-to-bag.service';
import { LanguageService } from 'src/app/shared/services';
import { CartService } from 'src/app/cart/cart.service';
import { ToastrService } from 'ngx-toastr';
import { ApiEndPoint } from 'src/app/shared/enums/api-end-point.enum';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddToBagPopupComponent } from 'src/app/shared/components/add-to-bag-popup/add-to-bag-popup.component';
import { WriteReviewComponent } from 'src/app/shared/components/write-review/write-review.component';
import { ReviewSearchComponent } from 'src/app/shared/components/review-search/review-search.component';

@Component({
  selector: 'app-brand-detail',
  templateUrl: './brand-detail.component.html',
  styleUrls: ['./brand-detail.component.css'],
})
export class BrandDetailComponent implements OnInit {
  totalCartItems: any;
  quantityValue: Array<Object> = [
    { value: 1 },
    { value: 2 },
    { value: 3 },
    { value: 4 },
    { value: 5 },
  ];
  review: any;
  getResponse: any;
  cartId: any;
  data = {};
  quantityValueForcart: any = 1;
  brandProductDetails: any;
  cartValue: any = 0;
  baseImageUrl = environment.baseImageUrl;
  myThumbnail: string;
  modalRef: BsModalRef;
  currentLang: any;
  cart: any;
  cartItems: any;
  subTotal: any;
  shippingTotal: any;
  cartTotalAmmount: any;
  totalDiscounts: any;
  cartTotal: any;
  constructor(
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private productDetailService: ProductDetailService,
    private menuService: MenuService,
    private modalService: BsModalService,
    private addToBagService: AddToBagService,
    private languageService: LanguageService,
    private router: Router,
    private cartService: CartService,
    private toastr: ToastrService,
    private dialog: MatDialog


  ) {
    // for translator
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });

    // For Brand details
    this.route.params.subscribe((res: any) => {
      {
        this.spinner.show();
        this.productDetailService
          .getProductInfoById(atob(res['id']))
          .subscribe((res: any) => {
            if (res.status) {
              this.brandProductDetails = res.data;
              this.review = res.data.review;
              // console.log(this.review);
              // console.log('brandProductDetails', this.brandProductDetails);
              this.myThumbnail =
                this.baseImageUrl + this.brandProductDetails.imagePaths[0];
              this.spinner.hide();
            } else {
              this.spinner.hide();
              this.menuService.sharedData(null);
            }
          });
      }
    });
  }

  ngOnInit(): void {
    this.getCartDetailById();

  }

  // Select Quantity or Item 

  // total Cart Item on Add to cart popup
  getCartDetailById() {
    this.spinner.show();
    let cartId = localStorage.getItem('cartId');
    this.cartService.getCartById(cartId).subscribe(
      res => {

        if (res['status'] == true) {
          this.getResponse = res;
          this.cart = res['data']['cart'];
          this.cartItems = res['data']['cartItems'];
          this.spinner.hide();
          // this.toastr.success(res['message']);
          this.addToBagService.addToBagItemCount(this.cartItems);
          this.addToBagService.cartValueCount(this.cart);
        }
        else {
          this.spinner.hide();
          // this.toastr.error(res['message']);
        }

      }
    )

  }

  onSelectQuantity(value: any) {
    this.quantityValueForcart = parseInt(value);
    this.quantityValueForcart = value;
  }

  // Add to Cart
  // openAddToCart(template: TemplateRef<any>, productData: any) {


  openAddToCart(productData: any) {

    // console.log(productData);
    this.cartId = localStorage.getItem('cartId');
    if (this.cartId == null) {
      this.data = {
        cartId: 0,
        quantity: this.quantityValueForcart,
        price: productData.price,
        productId: productData.productId,
      };
    }
    else {
      this.data = {
        // cartId: 0,
        cartId: localStorage.getItem('cartId'),
        quantity: this.quantityValueForcart,
        price: productData.price,
        productId: productData.productId,
      };
    }
    this.addToBagService.addToCart(this.data).subscribe((res: any) => {
      localStorage.setItem('cartId', res.cartId);
      this.totalCartItems = res.totalCartItems;
      this.addToBagService.cardItemCount(this.totalCartItems);
      localStorage.setItem('totalCount', JSON.stringify(res.totalCartItems));
      // this.addtocartService.cardItemCount(res.totalCartItems);
      this.getCartDetailById();
      this.toastr.success(res.message);
      this.openDialog();
    });
  }

  // countinue shoping
  getBrandsProductsList() {
    this.spinner.show();
    localStorage.removeItem('concernData');
    localStorage.removeItem('skinTypeData');
    localStorage.removeItem('ingredientsData');
    // this.menuService.sharedData(data);
    let data = localStorage.getItem('brandData');
    // console.log(data['brandId']);
    this.router.navigate(['/brand/brands-product-list/', btoa(data['brandId'])]);
  }

  // material dialog
  openDialog() {
    let config: MatDialogConfig<any> = {
      panelClass: 'myDialogClass'
    }
    const dialogRef = this.dialog.open(AddToBagPopupComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  // Write Review popup
  openWriteReview(){
    let config: MatDialogConfig<any> = {
      panelClass: 'myDialogClass'
    }
    const dialogRef = this.dialog.open(WriteReviewComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  // Review Search
  openReviewSearch(){
    let config: MatDialogConfig<any> = {
      panelClass: 'myDialogClass'
    }
    const dialogRef = this.dialog.open(ReviewSearchComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}



