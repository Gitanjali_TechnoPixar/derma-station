import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { LanguageService } from '../../../shared/services/language.service';
import { Subscription } from 'rxjs';
import { ProductService } from '../../../shared/services/product.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment.prod';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-brand-product-list',
  templateUrl: './brand-product-list.component.html',
  styleUrls: ['./brand-product-list.component.css'],
})
export class BrandProductListComponent implements OnInit {
  brandData: any;
  rate: number = 5;
  isShownodata: boolean = false;
  titleValue: any;
  isShowBrandData: boolean = false;
  isReadonly: boolean = true;
  currentLang: any;
  baseImageUrl = environment.baseImageUrl;
  subscription: Subscription;
  routeSub: any;
  inputValue: any;
  filterresData: any;
  productData = {};
  filter = [
    { SortBy: 'IsBestSeller', SortOrder: 'desc', name: 'Best Sellers' },
    { SortBy: 'CreatedOn', SortOrder: 'desc', name: 'New' },
    { SortBy: 'Price', SortOrder: 'asc', name: 'Price Low to High' },
    { SortBy: 'Price', SortOrder: 'desc', name: 'Price High to Low' },
    { SortBy: 'Rating', SortOrder: 'desc', name: 'Top Rated' },
  ]

  constructor(
    private router: Router,
    private productService: ProductService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    if (this.router.url == '/brand/brands-product-list') {
      this.router.navigateByUrl('brand/all-brands');
    }
  }

  ngOnInit(): void {
    this.spinner.show();
    this.route.params.subscribe((params) => {
      this.inputValue = atob(params['id']);
      this.productData = {
        pageNumber: 1,
        pageSize: 10,
        inputValue: atob(params['id']),
        // inputValue: atob(localStorage.getItem('countinueShoping')),
        sortBy: '',
        sortOrder: ''
      };
      localStorage.setItem('countinueShoping', JSON.stringify(this.inputValue));
      console.log(localStorage.getItem('countinueShoping'));

      // Use for future to send id console.log(params['id']);
      this.productService
        .getProductDetailsForBrand(this.productData)
        .subscribe((res: any) => {
          if (res.status === false) {
            this.isShownodata = true;
            this.spinner.hide();
          } else {
            this.isShownodata = false;
            this.brandData = res.data.dataList;
            this.titleValue = res.data.dataList;
            this.rate = res.data.dataList.rating;
            this.spinner.hide();
            if (res.message == "Data shown successfully") {
              this.toastr.success(res.message);
            }
            else {
              this.toastr.error(res.message);
            }
          }
        });
    });
  }

  // for filter Start

  onChangeFilter(evt) {
    let typeVal: string = evt.target.value;
    switch (typeVal) {
      case "Best Sellers":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "IsBestSeller",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForBrand(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.brandData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });

        break;
      case "New":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "CreatedOn",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForBrand(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.brandData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price Low to High":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "Price",
          sortOrder: "asc",
        };
        this.productService
          .getProductDetailsForBrand(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.brandData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price High to Low":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "Price",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForBrand(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.brandData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Top Rated":
        var productData = {
          pageNumber: 1,
          pageSize: 10,
          inputValue: this.inputValue,
          sortBy: "Rating",
          sortOrder: "desc",
        };
        this.productService
          .getProductDetailsForBrand(productData).subscribe(
            res => {
              // this.dataList = this.resData['dataList'];
              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.brandData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
    }

  }
  // filter End

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/brand/brands-detail/' + btoa(data.productId)]);
  }

  ngAfterViewInit() {
    window.scrollTo(0, 0);
  }

  // Pagingnation
  page: 1;
  totalLength:any;
  goTotop() {
    setTimeout(function () {
      var elmnt = document.getElementById('top');
      elmnt.scrollIntoView({
        behavior: 'smooth',
      });
    }, 0);
  }
}
