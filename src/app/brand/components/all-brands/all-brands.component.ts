import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { LanguageService, MenuService } from 'src/app/shared/services';
import { BrandService } from '../../services/brand.service';

@Component({
  selector: 'app-all-brands',
  templateUrl: './all-brands.component.html',
  styleUrls: ['./all-brands.component.css'],
})
export class AllBrandsComponent implements OnInit {
  allbrandsList: any;
  currentLang: any;

  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private languageService: LanguageService,
    private brandService: BrandService,
    private menuService: MenuService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit(): void {
    this.getBrands();
  }

  getBrands() {
    this.spinner.show();
    this.brandService.getAllBrands().subscribe((res: any) => {
      const sorted = res.data.listAllBrands.sort((a, b) =>
        a.brandName > b.brandName ? 1 : -1
      );
      const grouped = sorted.reduce((groups, contact) => {
        const letter = contact.brandName.charAt(0);
        groups[letter] = groups[letter] || [];
        groups[letter].push(contact);
        return groups;
      }, {});

      const result = Object.keys(grouped).map((key) => ({
        key,
        contacts: grouped[key],
      }));
      this.allbrandsList = result;
      this.spinner.hide();
    });
  }

  gotoDiv(content: any) {
    document.getElementById(content).scrollIntoView({ behavior: 'smooth' });
  }

  getBrandDetails(data: any) {
    this.menuService.sharedData(data);
    localStorage.setItem('sourceData', JSON.stringify(data));
    this.router.navigate(['/brand/brands-product-list/', btoa(data.brandId)]);
  }
}
