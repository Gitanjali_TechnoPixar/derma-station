import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllConcernsComponent, ConcernsProductListComponent } from '.';
import { ConcernComponent } from './concern.component';
import { ConcernsProductDetailComponent } from './components/concerns-product-detail/concerns-product-detail.component';

const routes: Routes = [
  {
    path: '',
    component: ConcernComponent,
  },
  {
    path: 'all-concerns',
    component: AllConcernsComponent,
  },

  {
    path: 'concerns-product-list',
    component: ConcernsProductListComponent,
  },
  {
    path: 'concerns-product-list/:id',
    component: ConcernsProductListComponent,
  },
  {
    path: 'concerns-details/:id',
    component: ConcernsProductDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConcernRoutingModule {}
