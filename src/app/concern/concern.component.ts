import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-concern',
  templateUrl: './concern.component.html',
  styleUrls: ['./concern.component.css'],
})
export class ConcernComponent implements OnInit {
  constructor(private router: Router) {
    if (this.router.url === '/concern') {
      this.router.navigateByUrl('concern/all-concerns');
    }
  }

  ngOnInit(): void {}
}
