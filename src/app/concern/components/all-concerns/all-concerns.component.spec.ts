import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllConcernsComponent } from './all-concerns.component';

describe('AllConcernsComponent', () => {
  let component: AllConcernsComponent;
  let fixture: ComponentFixture<AllConcernsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllConcernsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllConcernsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
