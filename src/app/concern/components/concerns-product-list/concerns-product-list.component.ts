import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MenuService } from '../../../shared/services/menu.service';
import { LanguageService } from '../../../shared/services/language.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/shared/services';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-concerns-product-list',
  templateUrl: './concerns-product-list.component.html',
  styleUrls: ['./concerns-product-list.component.css'],
})
export class ConcernsProductListComponent implements OnInit {
  concernData: any;
  rate: number = 5;
  titleValue: any;
  isShownodata: boolean = false;
  isReadonly: boolean = true;
  currentLang: any;
  baseImageUrl = environment.baseImageUrl;
  subscription: Subscription;
  sortBy: any;
  sortOrder: any;
  concernId: any;
  filterresData: any;
  pageNumber: any;
  pageSize: any;
  filterPageNumber: any;
  filterPageSize: any;
  filterconcernId: any;
  filter = [
    { SortBy: 'IsBestSeller', SortOrder: 'desc', name: 'Best Sellers' },
    { SortBy: 'CreatedOn', SortOrder: 'desc', name: 'New' },
    { SortBy: 'Price', SortOrder: 'asc', name: 'Price Low to High' },
    { SortBy: 'Price', SortOrder: 'desc', name: 'Price High to Low' },
    { SortBy: 'Rating', SortOrder: 'desc', name: 'Top Rated' },
  ]
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private toastr: ToastrService
  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    if (this.router.url == '/concern/concerns-product-list') {
      this.router.navigateByUrl('concern/all-concerns');
    }
  }
  ngOnInit(): void {
    this.spinner.show();
    this.route.params.subscribe((params) => {
      this.pageNumber = 1;
      this.pageSize = 10;
      this.concernId = atob(params['id']);
      this.sortBy = "";
      this.sortOrder = "";
      // Use for future to send id console.log(params['id']);
      this.productService
        .getProductDetailsForConcern(this.pageNumber, this.pageSize, this.concernId, this.sortBy, this.sortOrder)
        .subscribe((res: any) => {
          if (res.status === false) {
            this.spinner.hide();
            this.toastr.error(res.message);
            this.isShownodata = true;
          } else {
            this.isShownodata = false;
            this.concernData = res.data.dataList;
            this.titleValue = res.data.dataList;
            this.rate = res.data.dataList.rating;
            this.spinner.hide();
            if (res.message == "Data shown successfully") {
              this.toastr.success(res.message);
            }
            else {
              this.toastr.error(res.message);
            }
          }
        });
    });
  }

  // for filter Start

  onChangeFilter(evt) {
    let typeVal: string = evt.target.value;
    switch (typeVal) {
      case "Best Sellers":
        this.filterPageNumber = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.filterconcernId = this.concernId;
        this.sortBy = "IsBestSeller",
          this.sortOrder = "desc",
          this.productService
            .getProductDetailsForConcern(this.filterPageNumber, this.filterPageSize, this.filterconcernId, this.sortBy, this.sortOrder)
            .subscribe((res: any) => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.concernData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });

        break;
      case "New":
        this.filterPageNumber = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.filterconcernId = this.concernId;
        this.sortBy = "CreatedOn",
          this.sortOrder = "desc",
          this.productService
            .getProductDetailsForConcern(this.filterPageNumber, this.filterPageSize, this.filterconcernId, this.sortBy, this.sortOrder)
            .subscribe((res: any) => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.concernData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price Low to High":
        this.filterPageNumber = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.filterconcernId = this.concernId;
        this.sortBy = "Price",
          this.sortOrder = "asc",
          this.productService
            .getProductDetailsForConcern(this.filterPageNumber, this.filterPageSize, this.filterconcernId, this.sortBy, this.sortOrder)
            .subscribe((res: any) => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.concernData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Price High to Low":
        this.filterPageNumber = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.filterconcernId = this.concernId;
        this.sortBy = "Price",
          this.sortOrder = "desc",
          this.productService
            .getProductDetailsForConcern(this.filterPageNumber, this.filterPageSize, this.filterconcernId, this.sortBy, this.sortOrder)
            .subscribe((res: any) => {
              // this.dataList = this.resData['dataList'];

              // if (this.dataList.length === 0) {
              if (res['data'].length === 0) {
                this.isShownodata = true;
                this.spinner.hide();
                this.toastr.error(res['message']);
              } else {
                this.filterresData = res['data']['dataList'];

                this.concernData = this.filterresData;
                // this.totalLength=this.dataList.length;
                this.spinner.hide();
                if (res['message'] == "Data shown successfully") {
                  this.toastr.success(res['message']);
                  this.isShownodata = false;
                }
                else {
                  this.toastr.error(res['message']);
                  this.isShownodata = true;
                }
              }
            });
        break;
      case "Top Rated":
        this.filterPageNumber = this.pageNumber;
        this.filterPageSize = this.pageSize;
        this.filterconcernId = this.concernId;
        this.sortBy = "Rating";
        this.sortOrder = "desc";
        this.productService
          .getProductDetailsForConcern(this.filterPageNumber, this.filterPageSize, this.filterconcernId, this.sortBy, this.sortOrder)
          .subscribe((res: any) => {
            // this.dataList = this.resData['dataList'];
            // if (this.dataList.length === 0) {
            if (res['data'].length === 0) {
              this.isShownodata = true;
              this.spinner.hide();
              this.toastr.error(res['message']);
            } else {
              this.filterresData = res['data']['dataList'];

              this.concernData = this.filterresData;
              // this.totalLength=this.dataList.length;
              this.spinner.hide();
              if (res['message'] == "Data shown successfully") {
                this.toastr.success(res['message']);
                this.isShownodata = false;
              }
              else {
                this.toastr.error(res['message']);
                this.isShownodata = true;
              }
            }
          });
        break;
    }

  }
  // filter End

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/concern/concerns-details/', btoa(data.productId)]);
  }

  ngAfterViewInit() {
    window.scrollTo(0, 0);
  }
}
