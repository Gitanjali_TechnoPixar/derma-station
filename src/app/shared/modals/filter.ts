export interface Filters {
  sortOrder: string;
  sortField: string;
  pageNumber: number;
  pageSize: number;
  productCategoryId: any;
  searchQuery: string;
  filterBy: string;
}
