import { Component, OnInit, TemplateRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { MenuService } from '../../services/menu.service';
import { LanguageService } from '../../services/language.service';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale, arLocale } from 'ngx-bootstrap/chronos';
import { Router } from '@angular/router';
import { AddToBagService } from '../../services/add-to-bag.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
defineLocale('ar', arLocale);
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  personalCareConcernsList: any;
  personalCareItems: any;
  lang: string = 'en';
  currentLang: any = 'en';
  modalRef: BsModalRef;
  isCollapsed = true;
  isShowArabic: boolean = true;
  isShowEnglish: boolean = false;
  isShowBrowserBy = true;
  isShowBrand = true;
  ingredientList: any;
  allbrandlist: any;
  concernedList: any;
  item:any
  cartValue: any;
  cartValues: any;
  oneAtATime: boolean = true;
  allSkinType: any;
  allSkinCare: any;
  totalCartItems: any;
  locale = 'en';
  constructor(
    public translate: TranslateService,
    private menuService: MenuService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private localeService: BsLocaleService,
    private addtobagservice: AddToBagService,
    private modalService: BsModalService
  ) {
    if (this.translate.currentLang == 'ar') {
      this.localeService.use('ar');
      this.currentLang = 'ar';
      this.isShowEnglish = true;
      this.isShowArabic = false;
    } else {
      this.localeService.use('en');
      this.currentLang = 'en';
      this.isShowEnglish = false;
      this.isShowArabic = true;
    }
  }


  setLanguage(language: any) {
    this.languageService.shareLanguage(language);
    this.localeService.use(language);
    this.currentLang = language;
    if (language == 'ar') {
      this.isShowEnglish = true;
      this.isShowArabic = false;
    } else {
      this.currentLang = language;
      this.isShowEnglish = false;
      this.isShowArabic = true;
    }
    this.translate.setDefaultLang(language);
    localStorage.setItem('lang', language);
    this.translate.use(language);
    if (language == 'ar') {
      this.lang = 'en';
      document.body.setAttribute('dir', 'rtl');
      document.body.classList.add('arabic-page');
    } else {
      this.lang = 'ar';
      document.body.setAttribute('dir', 'ltr');
      document.body.classList.remove('arabic-page');
    }
  }
  ngOnInit(): void {
    this.browsebylist();
    this.getaddtobagCount();
    this.getPersonalCareList();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  browsebylist() {
    this.menuService.browsebylist().subscribe((res: any) => {
      this.allbrandlist = res.data.brandsList.listAllBrands;
      this.allSkinCare = res.data.skinCareList;
      this.allSkinType = res.data.skinTypeList;
      this.concernedList = res.data.concernedList;
      this.ingredientList = res.data.ingredientList;
    });
  }
  applyLocale(pop: any) {
    this.localeService.use(this.locale);
    pop.hide();
    pop.show();
  }


  getBrandsProductsList(data: any) {
    this.spinner.show();
    localStorage.removeItem('concernData');
    localStorage.removeItem('skinTypeData');
    localStorage.removeItem('ingredientsData');
    // this.menuService.sharedData(data);
    localStorage.setItem('brandData', JSON.stringify(data));
    this.router.navigate(['/brand/brands-product-list/', btoa(data.brandId)]);
  }
  // get personal care list data by Id
  getPesonalCareListData(data: any) {
    this.spinner.show();
    localStorage.removeItem('concernData');
    localStorage.removeItem('skinTypeData');
    localStorage.removeItem('ingredientsData');
    localStorage.setItem('personalCareData', JSON.stringify(data));
    this.router.navigate(['/personal/personal-care-list', btoa(data.subCategoryId)]);
  }

  getPesonalCareConcernData(data: any) {
    this.spinner.show();
    localStorage.removeItem('concernData');
    localStorage.removeItem('skinTypeData');
    localStorage.removeItem('ingredientsData');
    localStorage.removeItem('personalCareData');
    localStorage.setItem('concernData', JSON.stringify(data));
    this.router.navigate(['/personal/concern-list', btoa(data.concernId)]);
  }

  getConcernsProductsList(data: any) {
    this.spinner.show();
    localStorage.removeItem('brandData');
    localStorage.removeItem('skinTypeData');
    localStorage.removeItem('ingredientsData');
    this.menuService.sharedData(data);
    localStorage.setItem('concernData', JSON.stringify(data));
    this.router.navigate(['concern/concerns-product-list', btoa(data.concernId)]);
  }
  getSkinTypeProductsList(data: any) {
    this.spinner.show();
    localStorage.removeItem('concernData');
    localStorage.removeItem('brandData');
    localStorage.removeItem('ingredientsData');
    this.menuService.sharedData(data);
    localStorage.setItem('skinTypeData', JSON.stringify(data));
    this.router.navigate(['skin/skintype-product-list', btoa(data.skinTypeId)]);
  }

  getSkinCareProductsList(data: any) {
    this.spinner.show();
    localStorage.removeItem('concernData');
    localStorage.removeItem('brandData');
    localStorage.removeItem('ingredientsData');
    localStorage.setItem('skinTypeData', JSON.stringify(data));
    this.menuService.sharedData(data);
    this.router.navigate(['skin/skintype-product-list', btoa(data.subCategoryId)]);
    this.spinner.hide();
  }

  getIngredientsProductsList(data: any) {
    this.spinner.show();
    localStorage.removeItem('concernData');
    localStorage.removeItem('brandData');
    localStorage.removeItem('skinTypeData');
    this.menuService.sharedData(data);
    localStorage.setItem('ingredientsData', JSON.stringify(data));
    this.router.navigate([
      'ingredient/ingredients-product-list',
      btoa(data.ingredientId),
    ]);
  }

  getaddtobagCount() {
    this.spinner.show();
    this.addtobagservice.cartValue.subscribe((res: any) => {
      this.spinner.hide();
      this.cartValue = res;
      localStorage.setItem('cartValue', this.cartValue);
      localStorage.getItem('totalCount');
      if (localStorage.getItem('totalCount') == null) {
        this.cartValues = 0;
        this.cartValues= localStorage.removeItem('totalCount');
      }
      else if(this.cartValue){
        this.cartValues= this.cartValue;
        localStorage.removeItem('totalCount');
      }
      else {
        this.cartValues = localStorage.getItem('totalCount');
      }

    });
  }
  // get Personal care category data
  getPersonalCareList() {
    this.menuService.getPersonalCare().subscribe(
      res => {
        this.personalCareConcernsList = res['data']['personalCareConcernsList'];
        this.personalCareItems = res['data']['personalCareItems'];
      }
    )
  }
}