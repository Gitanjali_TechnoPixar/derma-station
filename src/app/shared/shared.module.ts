//Modules
import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { SharedRoutingModule } from './shared-routing.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MaterialModule } from '../material.module';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import {ProgressBarModule} from "angular-progress-bar"

//Shared Components
import {
  CategoriesComponent,
  BestSellerComponent,
  ClientsComponent,
  ProductComponent,
  KeepInTouchComponent,
  BannerComponent,
  BenefitsComponent,
  TestimonialsComponent,
  OurProductsComponent,
  FilterUiComponent,
  AboutUsComponent,
  ContactUsComponent,
  PoliciesReturnsComponent,
  PrivacyPolicyComponent,
  ProductDetailComponent,
} from './components';

//Layout Componets
import { HeaderComponent, FooterComponent } from './layouts';
import { NgxPaginationModule } from 'ngx-pagination';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OnlyEnglishDirective } from './directives/onlyEnglish';
import { OnlyArbicDirective } from './directives/onlyArabic';
import { ProductFilterComponent } from './components/product-filter/product-filter.component';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { RatingModule, RatingConfig } from 'ngx-bootstrap/rating';
import { ReviewComponent } from './components/review/review.component';
import { AddToBagPopupComponent } from './components/add-to-bag-popup/add-to-bag-popup.component';
import { WriteReviewComponent } from './components/write-review/write-review.component';
import { ReviewSearchComponent } from './components/review-search/review-search.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    CategoriesComponent,
    BestSellerComponent,
    ClientsComponent,
    ProductComponent,
    KeepInTouchComponent,
    BannerComponent,
    BenefitsComponent,
    TestimonialsComponent,
    OurProductsComponent,
    FilterUiComponent,
    AboutUsComponent,
    ContactUsComponent,
    PrivacyPolicyComponent,
    ProductDetailComponent,
    PoliciesReturnsComponent,
    OnlyEnglishDirective,
    OnlyArbicDirective,
    ProductFilterComponent,
    ReviewComponent,
    AddToBagPopupComponent,
    WriteReviewComponent,
    ReviewSearchComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedRoutingModule,
    CarouselModule,
    TranslateModule,
    MaterialModule,
    CollapseModule,
    NgxSpinnerModule,
    BsDropdownModule,
    NgxPaginationModule,
    AccordionModule,
    NgxImageZoomModule,
    BsDatepickerModule,
    RatingModule,
    ProgressBarModule,
  ],

  exports: [
    HeaderComponent,
    FooterComponent,
    CategoriesComponent,
    BestSellerComponent,
    ClientsComponent,
    KeepInTouchComponent,
    ProductComponent,
    BannerComponent,
    BenefitsComponent,
    TestimonialsComponent,
    OurProductsComponent,
    ProductFilterComponent,
    CarouselModule,
    NgxImageZoomModule,
    TranslateModule,
    FilterUiComponent,
    OnlyEnglishDirective,
    OnlyArbicDirective,
    NgxPaginationModule,
    RatingModule,
    ReviewComponent,
    ProgressBarModule,

  ],
})
export class SharedModule {}
