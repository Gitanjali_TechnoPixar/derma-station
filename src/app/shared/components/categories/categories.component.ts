import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment.prod';
import { ProductService } from '../../services/product.service';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { LanguageService } from '../../services';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css'],
})
export class CategoriesComponent implements OnInit {
  currentLang: any;
  productListForCategory: [];
  dataList: any;
  baseImageUrl = environment.baseImageUrl;
  // baseImageUrl: 'https://dermaquestapi.azurewebsites.net/';

  customOptions: OwlOptions = {
    loop: false,
    autoplay: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,

    dots: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 2,
      },
      740: {
        items: 3,
      },
      940: {
        items: 3,
      },
    },
    nav: false,
  };



  constructor(public translate: TranslateService,
    private languageService: LanguageService,
    private productService: ProductService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {

    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    this.spinner.show();
  }

  switchLanguage(language: string) {
    this.translate.use(language);
  }

  ngOnInit(): void {
    this.browseCategory();
  }
  category: {
    pageNumber: '1', pageSize: 1, inputValue: 1
  }
  browseCategory() {
    this.spinner.show();
    this.productService.getAllProductCategory().subscribe(
      res => {
        this.dataList = res['data']['result'];
        this.spinner.hide();
      });

  }

  productDetails(data: any) {
    let inputValue = data.categoryId
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    switch (inputValue) {
      case 1: {
        this.router.navigate(['/skin/all-skintypes']);
        break;
      }
      case 2: {
        this.router.navigate(['/hair-care/all-hair-care']);

        break;
      }
      case 3: {
        this.router.navigate(['/makeup/all-makeup']);

        break;
      }
      case 6: {
        this.router.navigate(['/personal/all-personal-care']);
        break;
      }
      case 12: {
        this.router.navigate(['/body-bath/shop-all-body-bath']);
        break;
      }
      case 13: {
        this.router.navigate(['/pro-collagen/shop-all-pro-collagen']);
        break;
      }
      default: {
        break;
      }
    }
  }


}



