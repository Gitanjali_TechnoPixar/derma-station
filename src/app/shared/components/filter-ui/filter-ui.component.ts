import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filter-ui',
  templateUrl: './filter-ui.component.html',
  styleUrls: ['./filter-ui.component.css'],
})
export class FilterUiComponent implements OnInit {
  constructor() {}

  filter=[
    {name:'Best Sellers', SortBy :'IsBestSeller', SortOrder:'desc' },
    {name:'New', SortBy :'CreatedOn', SortOrder:'desc' },
    {name:'Price Low to High', SortBy :'Price', SortOrder:'asc' },
    {name:'Price High to Low', SortBy :'Price', SortOrder:'desc' },
    {name:'Top Rated', SortBy :'Rating', SortOrder:'desc' },
  ]
  ngOnInit(): void {}
}
