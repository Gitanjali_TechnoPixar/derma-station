import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoliciesReturnsComponent } from './policies-returns.component';

describe('PoliciesReturnsComponent', () => {
  let component: PoliciesReturnsComponent;
  let fixture: ComponentFixture<PoliciesReturnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoliciesReturnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoliciesReturnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
