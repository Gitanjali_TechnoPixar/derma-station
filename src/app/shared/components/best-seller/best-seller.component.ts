import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment.prod';
import { AddToBagService, LanguageService, ProductService } from '../../services';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { CartService } from 'src/app/cart/cart.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { AddToBagPopupComponent } from '../add-to-bag-popup/add-to-bag-popup.component';

@Component({
  selector: 'app-best-seller',
  templateUrl: './best-seller.component.html',
  styleUrls: ['./best-seller.component.css'],
})
export class BestSellerComponent implements OnInit {
  getResponse:any;
  cartId: any;
  cart:any;
  cartItems:any;
  data = {};
  quantityValueForcart: number = 1;
  totalCartItems: any;
  productListForBestSellers: [];
  currentLang: any;
  baseImageUrl = environment.baseImageUrl;
  customOptions: OwlOptions = {
    loop: false,
    autoplay: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 2,
      },
      740: {
        items: 3,
      },
      940: {
        items: 3,
      },
    },
    nav: false,
  };
  constructor(
    public translate: TranslateService,
    private productService: ProductService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,
    private addToBagService: AddToBagService,
    private router: Router,
    private toastr: ToastrService,
    private cartService: CartService,
    public dialog: MatDialog

  ) {
    // this.spinner.show();
    this.productService.getProductListForBestSellers().subscribe((res: any) => {
      this.productListForBestSellers = res.data;
      this.spinner.hide();
    });

    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
    this.spinner.show();
  }

  ngOnInit(): void {
   }

  switchLanguage(language: string) {
    this.translate.use(language);
  }

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/brand/brands-detail/' + btoa(data.productId)]);
  }

  // Add to Cart
  // openAddToCart(template: TemplateRef<any>, productData: any) {

  openAddToCart(productData: any) {
    // this.modalRef = this.modalService.show(template);
    // console.log(productData);
    this.cartId = localStorage.getItem('cartId');
    if (this.cartId == null) {
      this.data = {
        cartId: 0,
        quantity: this.quantityValueForcart,
        price: productData.price,
        productId: productData.productId,
      };
    }
    else {
      this.data = {
        // cartId: 0,
        cartId: localStorage.getItem('cartId'),
        quantity: this.quantityValueForcart,
        price: productData.price,
        productId: productData.productId,
      };
    }
    this.addToBagService.addToCart(this.data).subscribe((res: any) => {
      localStorage.setItem('cartId', res.cartId);
      this.totalCartItems = res.totalCartItems;
      this.addToBagService.cardItemCount(this.totalCartItems);
      localStorage.setItem('totalCount', JSON.stringify(res.totalCartItems));
      // this.addtocartService.cardItemCount(res.totalCartItems);
      this.getCartDetailById();
      this.toastr.success(res.message);
      this.openDialog();
    });
  }

  // End Add To Cart

  // Add To Bag All product On Popup

  getCartDetailById() {
    this.spinner.show();
    let cartId = localStorage.getItem('cartId');
    this.cartService.getCartById(cartId).subscribe(
      res => {
        if (res['status'] == true) {
          this.getResponse = res;
          this.cart = res['data']['cart'];
          this.cartItems = res['data']['cartItems'];
          this.spinner.hide();
          // this.toastr.success(res['message']);
          this.addToBagService.addToBagItemCount(this.cartItems);
          this.addToBagService.cartValueCount(this.cart);
        }
        else {
          this.spinner.hide();
          // this.toastr.error(res['message']);
        }

      }
    )

  }

  // material dialog
  openDialog() {
    let config : MatDialogConfig<any> = {
      panelClass:'myDialogClass'
    }
    const dialogRef = this.dialog.open(AddToBagPopupComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
