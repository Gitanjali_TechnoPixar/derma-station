import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment.prod';
import { AddToBagService, LanguageService } from '../../services';

@Component({
  selector: 'app-add-to-bag-popup',
  templateUrl: './add-to-bag-popup.component.html',
  styleUrls: ['./add-to-bag-popup.component.css']
})
export class AddToBagPopupComponent implements OnInit {
  baseImageUrl = environment.baseImageUrl;

  cartId: any;
  data = {};
  quantityValueForcart: number = 1;
  totalCartItems: any;
  cart: any;
  cartItems: any[] = [];
  currentLang: any;
  sampleRes:any;
  constructor(
    private addToBagService: AddToBagService,
    private spinner: NgxSpinnerService,
    private languageService: LanguageService,

  ) {
    this.languageService.currentLangauge.subscribe((res: any) => {
      if (res == null || res == 'en') {
        this.currentLang = 'en';
      } else {
        this.currentLang = res;
      }
    });
  }

  ngOnInit(): void {
    this.getAddTobagItem();
  }
  // Add to bag popup data
  getAddTobagItem() {
    this.spinner.show();
    this.addToBagService.allItemValue.subscribe((res: any) => {
      this.cartItems = res;
      this.freeSample();
      this.addToBagService.allcartValue.subscribe((res) => {
        this.cart = res;
      })
      this.spinner.hide();
    });
  }

  freeSample(){
    this.spinner.show();
    this.addToBagService.getFreeSamples().subscribe(
      res=>{
        console.log(res);
        this.sampleRes=res['data'];
        this.spinner.hide();
      }
    )
  }
}

