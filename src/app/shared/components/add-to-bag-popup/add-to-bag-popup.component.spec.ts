import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToBagPopupComponent } from './add-to-bag-popup.component';

describe('AddToBagPopupComponent', () => {
  let component: AddToBagPopupComponent;
  let fixture: ComponentFixture<AddToBagPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddToBagPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToBagPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
