import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ContactUs } from '../../modals/contactus';
import { ContactUsService } from '../../services/contact-us.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css'],
})
export class ContactUsComponent implements OnInit {
  contactusForm: FormGroup;
  submitted = false;
  contactus: ContactUs;
  constructor(
    private formBuilder: FormBuilder,
    private contactService: ContactUsService,
    private toaster: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.contactusForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      firstNameInAr: ['', Validators.required],
      lastName: ['', Validators.required],
      lastNameInAr: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      emailInAr: [''],
      phoneNumber: ['', Validators.required],
      message: ['', Validators.required],
    });
  }

  get errorMsg() {
    return this.contactusForm.controls;
  }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;
    if (this.contactusForm.invalid) {
      this.spinner.hide();
      return;
    } else {
      this.contactusForm.addControl('infoId', new FormControl(0));
      this.contactService
        .addContactInfo(this.contactusForm.value)
        .subscribe((res: any) => {
          if (res.status) {
            this.spinner.hide();
            this.router.navigateByUrl('/home');
            this.toaster.success(res.message);
          } else {
            this.spinner.hide();
          }
        });
    }
  }
}
