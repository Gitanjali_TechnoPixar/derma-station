import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { ProductService } from '../../services/product.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  constructor(
    private productService: ProductService,
    private toastr: ToastrService,
    private spinner:NgxSpinnerService
  ) {}

  ngOnInit() {
    this.getProductCategory();
  }

  getProductCategory() {
    this.spinner.show();
    this.productService.getProductCategory().subscribe((res: any) => {
      this.spinner.hide();
      if (res.status) {
        this.toastr.success(res.message);
      } else {
        this.toastr.error(res.message);
      }
    });
  }
}
