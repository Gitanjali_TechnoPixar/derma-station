import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProductService } from '../../services';

@Component({
  selector: 'app-our-products',
  templateUrl: './our-products.component.html',
  styleUrls: ['./our-products.component.css'],
})
export class OurProductsComponent implements OnInit {
  constructor(
    private productService:ProductService,
    private spinner: NgxSpinnerService,
    private router:Router

    ) {}
  productTypeRes:any;
  ngOnInit(): void {
    this.productCategory();
  }

  productCategory(){
    this.spinner.show();
    this.productService.getProductCategory().subscribe(
      res=>{
        this.productTypeRes=res['data']['list'];
        this.spinner.hide();
      }
    )
  }

  productDetails(data: any) {
    setTimeout(function () {
      var elmnt = document.getElementById('backtotop');
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }, 0);
    this.router.navigate(['/brand/brands-detail/' + btoa(data.productId)]);
  }
}
