import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContactUs } from '../modals/contactus';
import { environment } from '../../../environments/environment.prod';
import { ApiEndPoint } from '../enums/api-end-point.enum';

@Injectable({
  providedIn: 'root',
})
export class ContactUsService {
  constructor(private http: HttpClient) {}

  //Add Contact Information
  addContactInfo(contactData: ContactUs) {
    return this.http.post(
      environment.baseUrl + ApiEndPoint.addContactInfo,
      contactData
    );
  }
}
