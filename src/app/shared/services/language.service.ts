import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LanguageService {
  private lang = new BehaviorSubject(null);
  currentLangauge = this.lang.asObservable();

  constructor() {}

  //Shareble Language
  shareLanguage(data: string) {
    this.lang.next(data);
  }
}
