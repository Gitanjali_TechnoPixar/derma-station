import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { ApiEndPoint } from '../enums/api-end-point.enum';

@Injectable({
  providedIn: 'root',
})
export class AddToBagService {
  baseUrl = environment.baseUrl

  cartitemValue: any;
  constructor(private http: HttpClient) { }
  private cartSource = new BehaviorSubject(null);
  cartValue = this.cartSource.asObservable();
  cardItemCount(datas: string) {
    this.cartSource.next(datas);
  }

  //Add to Cart
  addToCart(data: any) {
    return this.http.post(environment.baseUrl + ApiEndPoint.addToCart, data);
  }

  private addToBagAllItem = new BehaviorSubject(null);
  allItemValue = this.addToBagAllItem.asObservable();

  addToBagItemCount(datas: any) {
    this.addToBagAllItem.next(datas);
  }
  // Cart Value

  private addToBagCartItem = new BehaviorSubject(null);
  allcartValue = this.addToBagCartItem.asObservable();
  cartValueCount(datas) {
    this.addToBagCartItem.next(datas);
  }

  getFreeSamples(){
   return this.http.get(`${this.baseUrl}Products/GetFreeSamples`)
  }
}
