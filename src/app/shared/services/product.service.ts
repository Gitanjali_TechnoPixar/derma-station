import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
import { ApiEndPoint } from '../enums/api-end-point.enum';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private dataSource = new BehaviorSubject(null);
  datatoShare = this.dataSource.asObservable();
  public baseUrl = environment.baseUrl;
  public baseImageUrl = environment.baseImageUrl;

  constructor(private http: HttpClient) {}

  sharedData(data: string) {
    this.dataSource.next(data);
  }

  encodeString(textString: string) {
    return window.btoa(textString);
  }
  decodeString(encodedString: string) {
    return window.atob(encodedString);
  }

  //GetProductCategory
  getProductCategory() {
    return this.http.get(environment.baseUrl + ApiEndPoint.getProductCategory);
  }

  //GetProductDetailForBrand
  getProductDetailsForBrand(data: any) {
    return this.http.post(
      environment.baseUrl + ApiEndPoint.getProductDetailsForBrand,
      data
    );
  }

  //GetProductDetailForConcerns
  getProductDetailsForConcern(pageNumber, pageSize, concernId, sortBy, sortOrder) {
    return this.http.get(`${this.baseUrl}Products/GetProductListForConcern?pageNumber=${pageNumber}&pageSize=${pageSize}&concernId=${concernId}&sortBy=${sortBy}&sortOrder=${sortOrder}`)

  }

  getProductDetailsForIngredient(data: any) {
    return this.http.post(
      environment.baseUrl + ApiEndPoint.getProductDetailsForIngredient,
      data
    );
  }

  //GetProductDetailForSkinType
  getProductDetailsForSkinType(data: any) {
    return this.http.post(
      environment.baseUrl + ApiEndPoint.getProductDetailsForSkinType,
      data
    );
  }

  //GetProductDetailForBestSelller
  getProductListForBestSellers() {
    // return this.http.get('https://dermaquestapi.azurewebsites.net/api/Products/GetProductListForBestSellers')
    return this.http.get(`${this.baseUrl}Products/GetProductListForBestSellers`)
  }

  // Get Product Lis for Browse Categories

  getAllProductCategory(){
    return this.http.get(`${this.baseUrl}Products/GetAllProductCategory`)
  }




}



